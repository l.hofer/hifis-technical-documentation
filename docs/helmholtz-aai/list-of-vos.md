# List of registered Virtual Organisations (VO)
This document lists the Virtual Organisations (VO) registered in the Helmholtz AAI. Currently, VOs can represent working groups or Helmholtz centres.

VO memberships of a logged in user are transmitted by the Helmholtz AAI via the group claim as [documented here](namespaces-registry.md).

Last update: 2021-10-26.

## VOs of Working groups
Click on the corresponding link if you want to apply for a membership in the respective VO. This will send a corresponding mail to the HIFIS helpdesk. 
Depending on the VO policies, you may need to provide a reason on why you would like to become member.

### Group VOs in [helmholtz.de namespace](namespaces-registry.md#urngeanthelmholtzde-registry)

The required minimum [Assurance](assurance.md) in all cases is [AARC Assam](https://aarc-community.org/guidelines/aarc-g021/) (approximately corresponding to DFN AAI Basic).

⏩ Scroll to the right to see more details. ⏩

| VO Name | group claim, prefix:<br>`urn:geant:helmholtz.de:group:` | Registered | Last Changed | Contact | Application link |
| - | - | - | - | - | - |
| Arbeitskreise |  |  |  |  | <a href="mailto:it@helmholtz.de">Apply</a> |
| AirShowerPhysics | `AirShowerPhysics` | 26.10.2021 | 26.10.2021 | Ralf Ulrich | <a href="mailto:it@helmholtz.de">Apply</a> |
| DLR-BW-IS-Pilot | `DLR-BW-IS-Pilot` | 17.03.2021 | 17.03.2021 | Yorck Schneider-Kühnle, Peter Kadlec | <a href="mailto:support@hifis.net?subject=[Apply_VO_general]">Apply</a> |
| DLR-nubes | `DLR-nubes` | 17.03.2021 | 17.03.2021 | Yorck Schneider-Kühnle, Peter Kadlec | <a href="mailto:support@hifis.net?subject=[Apply_VO_general]">Apply</a> |
| DLR-Pilot | `DLR-Pilot` | 26.11.2020 | 26.11.2020 | Yorck Schneider-Kühnle, Peter Kadlec | <a href="mailto:support@hifis.net?subject=[Apply_VO_general]">Apply</a> |
| EuU_ADVANCE |  |  |  |  | <a href="mailto:support@hifis.net?subject=[Apply_VO_general]">Apply</a> |
| EuU_SynCom |  |  |  |  | <a href="mailto:support@hifis.net?subject=[Apply_VO_general]">Apply</a> |
| HCDC | `HCDC` | 27.01.2021 | 27.01.2021 | Ulrike Kleeberg | <a href="mailto:support@hifis.net?subject=[Apply_VO_general]">Apply</a> |
| HDF |  |  |  |  | <a href="mailto:support@hifis.net?subject=[Apply_VO_general]">Apply</a> |
| Head_Office |  |  |  |  | <a href="mailto:it@helmholtz.de">Apply</a> |
| HelmholtzDataHub | `HelmholtzDataHub` | 26.11.2020 | 26.11.2020 | Sabine Barthlott | <a href="mailto:support@hifis.net?subject=[Apply_VO_general]">Apply</a> |
| Helmholtz_Klimaneutral | `Helmholtz_Klimaneutral` | 27.01.2021 | 27.01.2021 | Peter Jüstel | <a href="mailto:support@hifis.net?subject=[Apply_VO_general]">Apply</a> |
| Helmholtz_Research_Software_Forum |  |  |  |  | <a href="mailto:support@hifis.net?subject=[Apply_VO_general]">Apply</a> |
| hereon_ksr | `hereon_ksr` | 10.09.2021 | 10.09.2021 | Burkhardt Rockel, Philipp Sommer | <a href="mailto:support@hifis.net?subject=[Apply_VO_general]">Apply</a> |
| HIFIS | `HIFIS` | 26.11.2020 | 26.11.2020 | Annette Spicker, Uwe Jandt | <a href="mailto:support@hifis.net?subject=[Apply_VO_general]">Apply</a> |
| HIP | `HIP` | 10.03.2021 | 10.03.2021 | Patrick Fuhrmann | <a href="mailto:support@hifis.net?subject=[Apply_VO_general]">Apply</a> |
| HMC | `HMC` | 26.11.2020 | 26.11.2020 | Emanuel Söding | <a href="mailto:support@hifis.net?subject=[Apply_VO_general]">Apply</a> |
| HZI-HIRI | `HZI-HIRI` | 26.11.2020 | 26.11.2020 | Michael Kütt | <a href="mailto:support@hifis.net?subject=[Apply_VO_general]">Apply</a> |
| HZI-Limesurvey | `HZI-Limesurvey` | 10.09.2021 | 10.09.2021 | Hendrik Eggers | <a href="mailto:support@hifis.net?subject=[Apply_VO_general]">Apply</a> |
| Inkubator_Digitalisierung_WP1 | `Inkubator_Digitalisierung_WP1` | 27.01.2021 | 27.01.2021 | Uwe Konrad, Ants Finke | <a href="mailto:support@hifis.net?subject=[Apply_VO_general]">Apply</a> |
| Inkubator_Digitalisierung_WP4 | `Inkubator_Digitalisierung_WP4` | 28.01.2021 | 28.01.2021 | Volker Gülzow | <a href="mailto:support@hifis.net?subject=[Apply_VO_general]">Apply</a> |
| Inkubator_contact_management | `Inkubator_contact_management` | 25.02.2021 | 25.01.2021 | Annette Spicker | <a href="mailto:support@hifis.net?subject=[Apply_VO_general]">Apply</a> |
| KODA_AG_MS_Alternativen | `KODA_AG_MS_Alternativen` | 05.03.2021 | 05.03.2021 | Ants Finke | <a href="mailto:support@hifis.net?subject=[Apply_VO_general]">Apply</a> |
| MOSES | `MOSES` | 26.10.2021 | 26.10.2021 | Ute Weber, Claudia Schütze | <a href="mailto:support@hifis.net?subject=[Apply_VO_general]">Apply</a> |
| POF4_Matter_MT_DMA_ST3 |  |  |  |  | <a href="mailto:support@hifis.net?subject=[Apply_VO_general]">Apply</a> |
| POF4_Topic4_Management | `POF4_Topic4_Management` | 27.01.2021 | 27.01.2021 | Juliane Petersen, Marcus Lange | <a href="mailto:support@hifis.net?subject=[Apply_VO_general]">Apply</a> |
| POF4_Topic6_Management |  |  |  |  | <a href="mailto:support@hifis.net?subject=[Apply_VO_general]">Apply</a> |
| SeroHub | `SeroHub` | 26.11.2020 | 26.11.2020 | Stephan Glöckner | <a href="mailto:support@hifis.net?subject=[Apply_VO_general]">Apply</a> |
| UFZ-IT | `UFZ-IT` | 27.01.2021 | 27.01.2021 | Thomas Schnicke | <a href="mailto:support@hifis.net?subject=[Apply_VO_general]">Apply</a> |

### Group VOs in [h-df.de namespace](namespaces-registry.md#urngeanth-dfde-registry)

| VO Name | group claim, prefix:<br>`urn:geant:h-df.de:group:` | Registered | Last Changed | Contact | Required [Assurance](assurance.md) | Application link |
| - | - | - | - | - | - | - |
|  | `HDF` | 31.08.2018 | 31.08.2020 | Sander Apweiler |  |  |
| hdf-cloud-JSC | `hdf-cloud-JSC` | 14.11.2018 | 14.11.2018 | Sander Apweiler | RAF Cappuccino | <a href="mailto:support@hifis.net?subject=[Apply_VO_general]">Apply</a> |
| IMK-TRO-EWCC | `IMK-TRO-EWCC` | 15.01.2019 | 15.01.2019 | Sundermann | HDF-DFN (or RAF Cappuccino) | <a href="mailto:support@hifis.net?subject=[Apply_VO_general]">Apply</a> |
| lsdf\_admin | `lsdf_admin` | 30.04.2020 | 30.04.2020 | Sundermann | RAF Cappuccino | <a href="mailto:support@hifis.net?subject=[Apply_VO_general]">Apply</a> |
| m-team | `m-team` | 11.02.2020 | 11.02.2020 | Marcus Hardt | RAF Cappuccino | <a href="mailto:support@hifis.net?subject=[Apply_VO_general]">Apply</a> |
| MyExampleCollab | `MyExampleColab` | 22.08.2018 | 22.08.2018 | Marcus Hardt | IGTF Dogwood | <a href="mailto:support@hifis.net?subject=[Apply_VO_general]">Apply</a> |
| wlcg-test | `wlcg-test` | 18.04.2018 | 18.04.2018 | Marcus Hardt | IGTF Dogwood | <a href="mailto:support@hifis.net?subject=[Apply_VO_general]">Apply</a> |

---

## VOs representing Helmholtz centres

The VO of the centres and Helmholtz-members are managed automatically by authentication via the IdPs of the centres.

The required [Assurance](assurance.md) in all cases is RAF Cappuccino.

| VO Name           |  group claim, prefix:<br>`urn:geant:helmholtz.de:group:`      |  Registered  |  Last Changed  |
| ----------------- | ----------------------------------------------------------------------- | - | -  |
|  AWI    | `AWI`   | 22.07.2020 | 22.07.2020  |
|  CISPA  | `CISPA` | 22.07.2020 | 22.07.2020  |
|  DESY   | `DESY`  | 22.07.2020 | 22.07.2020  |
|  DKFZ   | `DKFZ`  | 22.07.2020 | 22.07.2020  |
|  DLR    | `DLR`   | 22.07.2020 | 22.07.2020  |
|  DZNE   | `DZNE`  | 22.07.2020 | 22.07.2020  |
|  FZJ    | `FZJ`   | 22.07.2020 | 22.07.2020  |
|  GEOMAR | `GEOMAR`| 22.07.2020 | 22.07.2020  |
|  GFZ    | `GFZ`   | 22.07.2020 | 22.07.2020  |
|  GSI    | `GSI`   | 22.07.2020 | 22.07.2020  |
|  hereon | `hereon`| 22.07.2020 | 13.04.2021  |
|  HMGU   | `HMGU`  | 22.07.2020 | 22.07.2020  |
|  HZB    | `HZB`   | 22.07.2020 | 22.07.2020  |
|  HZDR   | `HZDR`  | 22.07.2020 | 22.07.2020  |
|  HZI    | `HZI`   | 22.07.2020 | 22.07.2020  |
|  Helmholtz-GS | `Helmholtz-GS` | 22.07.2020 | 22.07.2020  |
|  Helmholtz-member | `Helmholtz-member` | 22.07.2020 | 22.07.2020  |
|  KIT    | `KIT`   | 22.07.2020 | 22.07.2020  |
|  MDC    | `MDC`   | 22.07.2020 | 22.07.2020  |
|  UFZ    | `UFZ`   | 22.07.2020 | 22.07.2020  |

---

## Other supported VOs

The Helmholtz AAI does support some other VOs as well.

| VO Name           |  group claim      |  Registered  |  Last Changed  |
| ----------------- | ----------------------------------------------------------------------- | - | -  |
| PUNCH4NFDI        | `urn:geant:dfn.de:nfdi.de:punch:group:PUNCH4NFDI` | 05.08.2021 | 05.08.2021 |
