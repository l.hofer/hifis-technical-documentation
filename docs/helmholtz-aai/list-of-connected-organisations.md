# List of Connected Organisations
The Helmholtz AAI is connected to huge set of organisations which allows authentication, using the organisational accounts.

## eduGAIN
The Helmholtz AAI participates eduGAIN, which offers user authentication with the organisational account across the whole world. The organisation needs to participate eduGAIN, too. At the moment more than 4000 organisations participate eduGAIN.

## Helmholtz Centres
The Helmholtz AAI focuses but do not limit to Helmholtz centres. In oposit to other organisation, some further information about the connection to Helmholtz centres are gathered here.

Additionally, it is listed if the centres can be found/filtered by their shortname.

| Center  | IdP present  | in DFN AAI | User(s) | shortname provided |
|:-------:|:----:|:--------:|:----:|:---:|
| AWI     | yes  | advanced | yes | yes |
| CISPA   | yes  | basic    | yes | yes |
| DESY    | yes  | advanced | yes | yes |
| DKFZ    | yes  | advanced | yes | yes |
| DLR     | yes  | advanced | yes | yes |
| DZNE    |      | no       | no  | no  |
| FZJ     | yes  | advanced | yes | yes |
| Head Office | yes | advanced | yes  | no  |
| GEOMAR  | yes  | advanced | yes | yes |
| GFZ     | yes  | basic    | yes | yes |
| GSI     |      | no       | no  | no  |
| hereon  | yes  | basic    | yes | yes |
| HMGU    | yes  | advanced | yes | yes |
| HZB     | yes  | advanced | yes | yes |
| HZDR    | yes  | advanced | yes | yes |
| HZI     | yes  | advanced | yes | yes |
| KIT     | yes  | advanced | yes | yes |
| MDC     | yes  | advanced | yes | no  |
| UFZ     | yes  | advanced | yes | yes |

## social identity providers
Beside the organisational accounts the Helmholtz AAI supports some social identity providers (IdPs). Those IdPs can be used by users, where the organisation does not participate eduGAIN. Accounts, authenticated with socials IdPs, may have a lower assurance then organisation based accounts. A lower assurance may lower the set of available services or lower the functionality on services. The available social IdPs ares:
* GitHub
* Google
* ORCID
