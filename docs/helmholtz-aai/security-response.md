# Security Incident Response

Following our [incident response policy](policies/helmholtz-aai/02_HIFIS-SIRP.pdf),
Helmholtz AAI operates several mailinglists for incident response:

- <security@hifis.net>: This is the
  Infrastructure Security Contact point, which is open for anybody to post.
  Subscribers to this list are e.g.:
    - Contacts for the SP-IdP-Proxy (unity @ FZJ)
    - Service's site-security contacts (i.e. the CERTs of each site)

In case of an incident, this Infrastructure Security Contact point will
appoint and collaborate with the Security Incident Response Coordinator
for the given incident.

# SIRTFI

The Helmholtz AAI uses [SIRTFI](https://refeds.org/sirtfi), the Security Incident Response Trust Framework for Federated Identity, to communicate and collaborate with other organisations and instances, which are involved in the security incident.
