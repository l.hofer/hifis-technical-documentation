# Hints for Identity Providers (IdP)

## How to join Helmholtz AAI as an IdP

Helmholtz AAI supports all IdPs from [eduGAIN](https://edugain.org/).

In order to collaborate and join as an identity provider, please join your national federation and participate eduGAIN.

The mandatory information about the attributes consumed by the Helmholtz AAI, besides the federation metadata, are documented [here in the attributes section](attributes.md).

---

## Configuration of Shibboleth 4 IdP for Attribute Query

!!!warning
    This information is preliminary.
    The documentation will be completed as soon as possible.

This is a collection of configuration files technically needed for Attribute Query response in Shibboleth 4.

For general Shibboleth configuration, see also [DFN Documentation :fontawesome-solid-external-link-alt:](https://doku.tid.dfn.de/de:shibidp:config-deprovisionierung).

### Specifics for Shibboleth 4


* Exemplary configuration files are deposited below.
* `schacUserStatus.properties` must be located in `conf/attributes/custom/`.
* Implementation of `schacUserStatus` in `attribute-resolver.xml` is based on Active Directory as LDAP-Backend and uses the [UserAccountControl attribute](https://docs.microsoft.com/en-us/troubleshoot/windows-server/identity/useraccountcontrol-manipulate-account-properties).
  This needs to be handled differently when using OpenLDAP!
  (See example `attribute-resolver.xml` in DFN Documentation.)
* There is an example for a CERN Login in `relying-party.xml`, maybe it helps for understanding.

### Configuration Files

* [`attribute-filter.xml`](Attribute_Query/attribute-filter.xml)
* [`attribute-resolver.xml`](Attribute_Query/attribute-resolver.xml)
* [`relying-party.xml`](Attribute_Query/relying-party.xml)
* [`schacUserStatus.properties`](Attribute_Query/schacUserStatus.properties)

### Questions?

If you have further questions, please contact <support@hifis.net>.
