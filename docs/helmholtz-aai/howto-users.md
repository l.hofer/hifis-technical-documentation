# Guidelines for Users

## Login workflow

To use a service connected to the Helmholtz AAI, you simply choose one of the offered
[Helmholtz Cloud Services](https://cloud.helmholtz.de/#/services). You may also refer to the list of [pilot services](../cloud-services/list-of-services.md).

When logging in to a service, just search for “Helmholtz AAI” and use your home institute’s credentials.

Please [refer to our pictured tutorial on how to access Cloud Services via Helmholtz AAI](https://aai.helmholtz.de/tutorial/2021/06/23/how-to-helmholtz-aai.html) for more details.


#### VO membership needed for some services

Some services will require you to be member of a [Virtual
Organisation](concepts.md#the-virtual-organisation-vo-concept) (VO). To become member, you need to be invited/added by the PI (=Administrator) of the VO.  This [list of VOs](list-of-vos.md) may help to find the appropriate contact.

If your [Home-IdP](#identity-providers) is from a Helmholtz centre and connected to the Helmholtz AAI, you are automatically member of the VO representing your organisation. This allows you to automatically gain access to certain services that allow all users from your home organisation.

------------
## Policies

As a user, you must only accept and abide by the Acceptable Use Policies (AUP) presented to you, either:

- VO AUP: when joining a VO, or

- SAUP: when using a service for the first time.

In addition, each VO you join and service you use which processes your personal data is required to provide a Privacy Policy (PP) and make it available to you.

You can find more information about the policies in use in the Helmholtz AAI [here](policies/README.md).

------------
## Technical

### How to log in with Helmholtz AAI

When you click your service, you will be redirected to the Helmholtz AAI community
proxy, powered by Unity. For using Unity you have to choose your home Identity
Provider (see below) at which you log in. 

#### First time login

On the first use you may need to verify your email address. Just follow the
procedures, it will work. Note that you need to click on "remember choice" at
several places to make login work smoother in subsequent attempts.

During the registration the first part of your email address is stored as
preferred username. You can update it in the [home interface](https://login.helmholtz.de/home).


#### Identity Providers

The AAI proxy does not manage identities by itself. For identities it trusts so called
Identity Providers (IdPs). As a user you must have an account with one of the
supported Identity Providers. Different classes of Identity Providers exist,
with pros and cons:

- "Home"-IdP: this is typically provided by your research centre or university.
  This is the place to which you get redirected in order to log in to a service.

- Social IdP: "social" networks like google or github also provide
  identities. However, these do not verify user-passports and do not have a
  work contract with their users. The services accessible with such identities may
  be limited.

### How to interact with Unity via `oidc-agent`

`oidc-agent` is a set of tools to manage OpenID Connect tokens and make them easily usable from the command line.

#### Installing the agent

For installing the agent follow the instructions on the website of `oidc-agent`:

<https://indigo-dc.gitbook.io/oidc-agent/installation>

A growing number of Linux distributions have `oidc-agent` already in their repository. If you use one of these distributions you may want to use the packaged version.

#### Create a Configuration for the Helmholtz AAI

`oidc-agent` comes with a number of pre-configured public client configurations. There is also a public client for the Helmholtz AAI which makes configuration easy.

-   In order to start the configuration process:
`oidc-gen --pub <shortname>`

-   Enter `https://login.helmholtz.de/oauth2/` as issuer.
-   Select the scopes you want to access or accept the preconfigured list.
-   Enter the desired redirection URLs (you have to add at least one - if you only plan to use the CLI agent, this doesn't really matter, so you can use e.g. `http://localhost:8080`)
-   Important: Login with your home IdP in the browser window which opens. This step is necessary to complete the account configuration.
-   as a final step, add a password for the generated configuration. You will need this password everytime, you add the configuration to `oidc-agent` so note down your passphrase carefully
- start `oidc-agent` and set the necessary environment variables with ``eval `oidc-agent` ``
- check if your configuration works with `oidc-token <short name of your configuration>`

#### Accessing user information

After configuring `oidc-agent` and logging in you can now start using it. You can, for example, request your users information with `curl`:

``curl -H "Authorization: Bearer `oidc-token <short name of your configuration>`" -X GET https://login.helmholtz.de/oauth2/userinfo``

If you want to have a nice, readable output, you can format the result with `jq` (you will probably have to install jq before that):

``curl -H "Authorization: Bearer `oidc-token <short name of your configuration>`" -X GET https://login.helmholtz.de/oauth2/userinfo | jq .``

This should give you something like:

```
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100  1227  100  1227    0     0   7967      0 --:--:-- --:--:-- --:--:--  7967
{
  "sub": "576a03d3-39a0-4f3a-b32a-f7b3bd29111c",
  "email_verified": true,
  "groups": [
    "/DESY",
    "/Helmholtz-member",
    "/HDF",
    "/"
  ],
  "preferred_username": "carsten.heidmann",
  "display_name": "Carsten Heidmann",
  "given_name": "Carsten",
  "eduperson_principal_name": "cheidman@desy.de",
  "eduperson_entitlement": [
    "urn:geant:h-df.de:group:HDF#login.helmholtz.de",
    "urn:geant:h-df.de:group:DESY#login.helmholtz.de",
    "urn:geant:h-df.de:group:Helmholtz-member#login.helmholtz.de"
  ],
  "ssh_key": "ssh-rsa AAAAB3NzaC1yc2EAAAABIwAAAQEAyqvhXPY0ETi7c954y3vV+c6bWdIihvrWokmv3GrndaFMyX2cKpzZofKXKdnK7Nv6TFJASCKeLqAyON0CGlE3ZgRw44bnJD5lWNN9uKInZ+EMKKyO4XqgTYoEIsGNTElyHjRw2hvArlQpP89ULnPWsVemdJza+8TTEMtTSVN/fX9ofOk63GJcuOpkH+x7CIWjc7QW5KmLc0Jc2+Xzu4IBmRKRXk9uaI/e7sFXPPLa+PHMtywxmVFhDANyElBSAeRmjo5RHJ1Azd08obMJMJCrfEKoZgLlfz5OpUiF6Ntqu00dSeIVvcyNEgXgfXPX55G5jTEwp7B1H7nakdUVH6e46Q== Carsten Heidmann (RSA)\n",
  "name": "Carsten Heidmann",
  "eduperson_unique_id": "576a03d339a04f3ab32af7b3bd29111c@login.helmholtz.de",
  "sn": "Heidmann",
  "eduperson_assurance": [
    "https://refeds.org/assurance/ATP/ePA-1m",
    "https://refeds.org/assurance/ID/unique",
    "https://refeds.org/assurance/IAP/low"
  ],
  "family_name": "Heidmann",
  "email": "carsten.heidmann@desy.de"
}
```
