# Registration of a Virtual Organisation (VO) 

We are working on a central place to request a Virtual Organisation (VO) and manage the mandatory information for this. Until this is in place, you have to request the VO via an email and provide the necessary information.

Send an email to [Helpdesk](mailto:support@hifis.net?subject=[VO request]),
providing the following information to register a new VO:

* VO name
* VO description
* VO manager
* VO acceptable use policy. Templates can be found here: [odt](policies/helmholtz-aai/04_HIFIS-AUP.odt) / [pdf](policies/helmholtz-aai/04_HIFIS-AUP.pdf).
* VO privacy policy, if needed. Templates can be found here: [odt](policies/helmholtz-aai/05_HIFIS-PP.odt) / [pdf](policies/helmholtz-aai/05_HIFIS-PP.pdf).

We will get in touch with you, while setting up the VO or if something is missing.


## Preconditions

You (as a VO admin) need to be able to authenticate with the [assurance](assurance.md) of RAF Cappuccino, i.e. you need to identify with your passport at your Identity Provider.

### Responsibility

As an administrator of a Virtual Organisation you take a substantial share of
responsibilities for a working process. The requirements come from the Services.
Many services have requirements on the quality of the user identity
[assurance](assurance.md) and on the general quality of the identity provider. 

Depending on the service (in this case those allow shell access or data storage)
this often requires the users to have shown a passport at their home-IdP and
also require the home-IdP to support certain security procedures.

### International Users

In Helmholtz AAI we want to enable users for which those criteria often aren't met.
Therefore, we offer the possibility to add all kinds of users to a VO, but we
require the VO admin to guarantee that an appropriate level of identity vetting
has taken place.
<!-- We are currently in the process of deriving a comprehensive
set of guidelines to help VO admins to do their job. -->

------------
### Further information about Policies

As defined in the top level policy, VO administrators have several tasks to
fulfil:

- Abide by the following policies:
    - [Helmholtz AAI Top Level Policy](policies/helmholtz-aai/00_HIFIS-Top-Level-Policy.pdf)
    - [Membership Management Policy](policies/helmholtz-aai/01_HIFIS-VOMMP.pdf)
    - [Security Incident Response Procedure](policies/helmholtz-aai/02_HIFIS-SIRP.pdf)
    - [Policy on the Processing of Personal Data](policies/helmholtz-aai/03_HIFIS-PPPD.pdf)

- If necessary, define AUP and PP policies for your VO by extending the following templates:
    - [Acceptable Use Policy Template (AUP-Template)](policies/helmholtz-aai/04_HIFIS-AUP.odt) / [PDF version](policies/helmholtz-aai/04_HIFIS-AUP.pdf)
    - [Privacy Policy Template (PP-Template)](policies/helmholtz-aai/05_HIFIS-PP.odt) / [PDF version](policies/helmholtz-aai/05_HIFIS-PP.pdf)

In most cases, a PP is not necessary if the VO is managed at unity and you do not additionally process any personal data.

