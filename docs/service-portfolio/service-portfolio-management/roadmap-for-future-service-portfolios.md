# Roadmap for future Service Portfolios

The roadmap for future Service Portfolios is kept up-to-date on the [HIFIS Website](https://hifis.net/roadmap/).

From Service Portfolio Side, it includes the following milestones:

* October 2020
    * Announcement of initial Helmholtz Cloud Service Portfolio
    * Roadmap for the Integration of the initial Services
* April 2021
    * Start evaluation of further Services
* December 2021
    * Process Framework for Helmholtz Cloud Service Portfolio finalized
* January 2022
    * Review of Initial Cloud Service Portfolio

With regards to the evaluation of further Services, the [service selection criteria used for the initial Service Portfolio](../initial-service-portfolio/how-services-are-evaluated.md) will be reviewed and adapted to the needs of future portfolios.


