# Current Services in the Service Portfolio

Last update: 2021-10-18.

The Helmholtz Cloud Service Portfolio includes:

- the **Service Pipeline**: includes all Services which passed the Exclusion criteria (asked for in the Application form) OR are part of the initial service portfolio -  but have not yet been fully integrated into Helmholtz Cloud.
- the **Service Catalogue** includes all services that are currently provided/ in operation. In the context of Helmholtz Cloud, the Service Catalogue is represented by the services offered in Helmholtz Cloud Portal (as "pilots" until the contract is signed by all Helmholtz centers)
- the **Retired services** are the services which were once operated but aren't anymore. In the context of Helmholtz Cloud, the retired Services are not visible in Helmholtz Cloud Portal anymore, but still exist as part of the service database (Plone) with the status "Retired".

| Service Pipeline | Service Catalogue | Retired Services |
| ------ | ------ | ------ | 
| OpenStack, Jülich (JuCloud) ; KIT ; DKFZ | OpenStack (HDF Cloud), Jülich |  |
| Storage (HDF), Jülich ; DESY | Mattermost, HZDR |  |
| GitLab, Jülich ; KIT ; GEOMAR | GitLab (HIFIS Software), HZDR |  |
| JupyterHub, DESY ; DKFZ ; HMGU | JupyterHub, Jülich |  |
| HAICORE (HAICU, HIP), Jülich | HAICORE (HAICU, HIP), KIT | |
| Singularity, Jülich ; KIT | Zammad (HIFIS Helpdesk), HZDR |  |
| Docker, DESY | bwSync&share (NextCloud), KIT |  |
| ShareLaTex, HZDR | Nextcloud (OnlyOffice), HZB, DESY |  |
| AWI Marketplace, AWI | B2Share (Invenio), Jülich |  |
| GPU compute Service, HZDR ; Jülich |  |  |
| JupyterHub notebooks on HPC, KIT |  |  |
| LimeSurvey, DKFZ ; HMGU |  |  |
| Ocean & climate Sensor Management, AWI |  |  |
| ODV, AWI |  |  |
| Redmine, HMGU |  |  |
| Rocket.Chat, Jülich |  |  |
| Rodare, HZDR |  |  |
