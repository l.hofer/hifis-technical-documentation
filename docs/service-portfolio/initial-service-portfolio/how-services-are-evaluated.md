# Evaluation of Services

For a full list of Criteria, as well as the full Weighting and Point Distribution Scheme, see [HIFIS Cloud Service Selection Criteria](../Corresponding-files/HIFIS-Cloud_service-selection-criteria-v8.pdf). Please note that the service selection criteria incl. Weighting and Point Distribution has been worked out prior to the start of the service selection process.

## Definition of Service Selection Criteria

A first draft of service selection criteria has been worked out by HIFIS Cloud Competence Center in April 2020 before starting with the service selection process. The criteria have been divided into three criteria types: exclusion criteria (if the service does not fulfill this criteria, it will be excluded from service selection), weighting criteria (if the service fulfills this criteria, it will earn additional points) and info criteria (do not influence the evaluation of a service but are rather informational). Furthermore, the criteria were attached to three categories: overall (or, in other words, general/organizational) criteria, technical criteria and service provider criteria This draft has been used as a basis for discussion with all HIFIS Cloud members who were interested in working out the criteria.

Approx. 12 HIFIS Cloud members participated in the service selection criteria discussion. The criteria have then been adapted to what was discussed and has been used as a basis for the development of further criteria details.

## Weighting of Criteria

The weighting of criteria (only applicable for weighting criteria) has been made up by using a matrix (see [HIFIS Cloud Comparison Matrix for Weighting of Service Selection Criteria](../Corresponding-files/HIFIS-Cloud_comparison-matrix-for-weighting-of-the-service-selection-criteria-v8.pdf)). This matrix compares every criteria with every single other criteria by indicating whether the criteria is more important (+), equally important/neutral (N) or less important (-) than the criteria it is compared to. The matrix has been filled out in a video conference in which the approx. 15 HIFIS cloud members participated.

We went through the matrix twice and indicated the importance of each criteria compared to all others. The algorithm behind the matrix subtracts – votes from + votes and divides the result by the total number of criteria. To get results between 0 and 1, the matrix adds 1 (to get positive values >0) and divides the total by 2 (to get values <1).

The resulting weighting has been fixed before starting the service selection.

## Definition of Point Distribution

The point distribution of weighting criteria means how to measure how good a service fulfills a weighting criterion. For the point distribution of weighting criteria, a range of 0-4 points has been chosen. There are three variants of point distribution, depending on the criteria:

- Service earns either 4 points for fulfilling a criterion or 0 points for not fulfilling it
- Service earns either 4, 2 or 0 points, depending on how good a service fulfills a criterion (e.g. serves one community (0 points), two communities (2 points) or more than two communities (4 points))
- Services earns 4,3,2,1,0 points, depending on which range (of time, percentage of total etc.) it belongs to (e.g. 81-100% of centers interested in a service: 4 points, 61-80% of centers interested in a service: 3 points, etc.)

The point distribution has been worked out in a video conference in which the approx. 14 HIFIS cloud members participated. The participants focused on choosing point distributions that can be measured as objectively as possible, using either the results from the service survey or the expertise of the service providers offering the services.
The resulting criteria list with weighting and point distributions worked out has been used as a basis for the following service selection.
