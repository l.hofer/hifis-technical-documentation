# How services are selected

## The service selection process

To get an overview over the whole service selection process in one presentation, please have a look at the [Service Selection Presentation](../Corresponding-files/HIFIS-Cloud_Service-Selection-Process-Presentation-v2.pptx).

The service selection process has been worked out to efficiently identify the services which fulfill the selection criteria best and which should consequently be integrated into Helmholtz cloud first. Starting with the list of candidate services made up during the two rounds of service surveys the service selection process is conducted in three iterations: 

- In the 1st Iteration all service selection criteria which can be evaluated with the information gained in the service survey rounds are applied. The services which earned more than 6 points of possible 7,72 points are taken into the 2nd iteration (56 services).
- In the 2nd Iteration all service selection criteria which require information that can easily be given by the potential service providers (without lots of detailed investigation) are applied. All services from 2nd iteration except for those without a service provider are taken into the 3rd Iteration (47 services).
- In the 3rd iteration all service selection criteria that require detailed service information from the potential service providers are applied. Service Canvas' for each service are filled out to gain detailed service information. After the 3rd iteration, 38 services were left to be integrated into Helmholtz cloud as the initial service portfolio.

Therefore, in each iteration the service providers were asked for further information on their services and, using this information to evaluate the service selection criteria, the service list was continously reduced over the iterations to finally result in the initial service portfolio: 

![Alt-Text](../Corresponding-files/HIFIS-Cloud_Service-Selection-Process-Visualization.png)

For an animated version of the Service Selection Process Visualization, see [HIFIS Cloud Service Selection Process - Animated in PowerPoint](../Corresponding-files/HIFIS-Cloud_Service-Selection-Process-Animated-in-PowerPoint.pptx)

For the distribution of service selection criteria to iterations, see [HIFIS Cloud Service Selection Criteria & Iterations](../Corresponding-files/HIFIS-Cloud_service-evaluation_criteria+iterations.pdf).

The service selection process has been presented to the Scientific Advisory Board (SAB) as well as the ICT Federation Board at the end of April 2020.

## The 1st Iteration of the service selection process

The 1st Iteration of the service selection process could be conducted after the 2nd Service Survey round was completed, starting in April 2020. The following criteria (see [How Services are evaluated](how-services-are-evaluated.md) for details on how service selection criteria was made up) were part of the 1st Iteration:

| # | Category | Criteria | Criteria Type | Unit | Weight | 4 | 3 | 2 | 1 | 0 |
| :- | :- | :- | :- | :- | :- | :- | :- | :- | :- | :- |
| 1 | Overhead | Service is provided for free | A | Yes/No | | | | | | |
| 2 | Overhead | Service is provided by a Helmholtz center | A | Yes/No | | | | | | | |
| 9 | Technical | Services are capable for Helmholtz cloud provisioning | A | Yes/No | | | | | | | |
| 3 | Overhead | Service explicitly supports scientists/ their processes | B | Yes/No | 0,56 | Yes | | | | No |
| 4 | Overhead | Broad range of centers is interested in using the service | B | No. of interest votes in Service Survey | 0,81 | 19-16 | 15-12 | 11-8 | 7-4 | 0-3 |
| 5 | Overhead | Broad range of communities/ user groups is interested in using the service | B | No. of interested Communities | 0,56 | >2 |   | 2 |   | 0-1 |
| 8 | Overhead | Service is named in the HIFIS proposal | I | Yes/No | | | | | | |
| 19 | Technical | Service is open source | I | Yes/No | | | | | | |

*Criteria Types: A - Exclusion criteria; B - Weighting criteria; I - Information*

The service list was evaluated using the named criteria and the resulting maximum points a service could earn in the 1st iteration were 7,72 points.
The services that did not fulfill exclusion criteria were excluded from the following iterations. Nevertheless, the excluded services were differentiated into three different lists since they required to be treated differently:
- List of service with an external (non-Helmholtz) service provider
       - Might get into negotiations with external service provider to find out about possible conditions (after initial service portfolio phase)
- List of services without a service provider
       - Might ask Helmholtz centers if they would like to provide some of these services (if the demand is accordingly high)
- List of other excluded services
       - Will not be further treated until they fulfill all exclusion criteria

The following 39 services from the 1st Iteration are postponed to the next service evaluation/ a future service portfolio:

| Service Type | Service | Service Provider | Service Category |
| ------ | ------ | ------ | ------ | 
| Large Data Transfer Service | Globus Toolkit | HMGU | Infrastructure Service |
| Large Data Transfer Service | UFTP (UNICORE FTP) | Jülich | Infrastructure Service |
| Analytic machines | HeAT Library | Jülich, DESY | Infrastructure Service |
| On demand compute (offering CPU and GPU ressources) | Community Service (Energy & Information) | KIT | Infrastructure Service |
| On demand compute (offering CPU and GPU ressources) | Community Service (Health) | DKFZ | Infrastructure Service |
| On demand compute (offering CPU and GPU ressources) | Community Service (Nuclear Physics) | GSI | Infrastructure Service |
| On demand compute (offering CPU and GPU ressources) | Rancher | Jülich | Infrastructure Service |
| On demand storage | Communitystorage (Earth & Environment) | AWI | Infrastructure Service |
| On demand storage | Communitystorage (Matter) | DESY, GSI | Infrastructure Service |
| On demand storage | Communitystorage (Health) | DKFZ | Infrastructure Service |
| On demand storage | Distributed Object Storage | DKFZ + HZDR | Infrastructure Service |
| Virtual Desktop Infrastructure | Virtual Desktop Service | UFZ, Jülich | Infrastructure Service |
| Application specific contact repository | Stack Overflow Team (local) | HZDR | Community/Science Service |
| R-Studio, R-Shiny, R-Project | R-Studio, R-Shiny, R-Project | HMGU, DKFZ, AWI | Community/Science Service |
| Electronic Laboratory Journal (LIMS) | OpenBIS | HZDR | Community/Science Service |
| Electronic Laboratory Journal (LIMS) | New HMGU System | HMGU | Community/Science Service |
| Data Repository with DOI Service | PANGAEA | AWI | Community/Science Service |
| Data Repository with DOI Service | ICAT | HZB | Community/Science Service |
| OpenStreetMap Services | OSM Nominatim (Geocoder/Reverse Geocoder) | Jülich | Community/Science Service |
| OpenStreetMap Services | OSM Overpass (OSM map data API) | Jülich | Community/Science Service |
| OpenStreetMap Services | OSM Tileserver (Webserver for map tiles) | Jülich | Community/Science Service |
| Sync&Share | B2DROP | Jülich | Collaboration Service |
| Discussion Channel, Chat | Jabber XMPP | HZDR | Collaboration Service |
| e-Learning / Instruction | Moodle | DKFZ | Collaboration Service |
| e-Learning / Instruction | ILIAS | HMGU | Collaboration Service |
| Proposal System | GATE | HZB | Collaboration Service |
| Proposal System | Door | DESY | Collaboration Service |
| Ticketing Tool, Issue Tracker | OTRS | Jülich | Collaboration Service |
| Ticketing Tool, Issue Tracker | Request Tracker | DESY | Collaboration Service |
| Ticketing Tool, Issue Tracker | Efecte | HMGU | Collaboration Service |
| Chemicals Administration | BIDS | HMGU | Collaboration Service |
| Sandstorm Microcontainer | Sandstorm | HMGU | Collaboration Service |
| Email/Calendar/Contacts | Entire hosting of mail server | DESY, Jülich | Other Service |

The following 52 services from the 1st iteration have no service provider (Status Quo: 22.04.2020):

| Service Type | Service | Service Category |
| ------ | ------ | ------ |
| Analytic machines | Binder Repositories | Infrastructure Service |
| Database as a Service | MySQL and Postgres | Infrastructure Service |
| Large Data Transfer Service | Aspera (Genomic) | Infrastructure Service |
| On demand storage | SpectrumProtect HSM | Infrastructure Service |
| Analytic machines | Communityservice | Infrastructure Service |
| On demand compute (offering CPU and GPU ressources) | ESX/ESXi | Infrastructure Service |
| Application specific contact repository (incl. Wiki/Chat) | Helmholtz Contact Exchange | Community/Science Service |
| Build service & package repository | docker hub | Community/Science Service |
| Electronic Laboratory Journal (LIMS) | LabFolder, Confluence, mediawiki, elabFTW, Medusa | Community/Science Service |
| Sample Management | Cell samples (Biology) | Community/Science Service |
| Apache Spark Cluster | Data Analytics Services | Community/Science Service |
| Study Conduction Service (observational studies)  | Study Conduction Service (observational studies) | Community/Science Service |
| Workflow Platform  | Eugene, Geneious | Community/Science Service |
| Workflow Platform | Galaxy Web Server | Community/Science Service |
| DOI Minting Service | Service to aquire DOI | Community/Science Service |
| Data Repository with DOI Service | GFZ Data Services | Community/Science Service |
| Data Management Plan | HGF EaE Specific DMP Tool | Community/Science Service |
| Sample Management | IGSN Service (Geo Science) | Community/Science Service |
| Sensor Management Service | Geo Sensor Management | Community/Science Service |
| Central Geodata Platform | Central Geodata Platform | Community/Science Service |
| Sync&Share with Cloud Office | Nextcloud with Collabora | Collaboration Service |
| Discussion Channel, Chat | Matrix (protocol) | Collaboration Service |
| Online Survey Service | Polls (NextCloud), RedCap | Collaboration Service |
| Project Websites | Confluence, Wordpress | Collaboration Service |
| Literature Management | Zotero | Collaboration Service |
| Ticketing Tool, Issue Tracker | Redmine | Collaboration Service |
| Voting | Electronic Voting | Collaboration Service |
| Publishing Repository | Invenio | Collaboration Service |
| Project Management with Wiki | Sheila, OnlyOffice Project, DECK (Nextcloud) | Collaboration Service |
| Process Management/ Workflow Engine / Web-Templates | Lucom Interaction Platform, FMS | Collaboration Service |
| Voice Chat | Jabber eJabberDaemon, Lync Federation, Chat over IMAP | Collaboration Service |
| Norm Database | Norm Database | Collaboration Service |
| e-Learning / Instruction | OpenOLAT, Eplas | Collaboration Service |
| Chemicals Administration | GESKAT | Collaboration Service |
| Cloud CAD System | onshape, simscale | Collaboration Service |
| SW-Package - Catalog | SW-Package - Catalog | Collaboration Service |
| Mailing list tool | MailMan | Collaboration Service |
| Continous Improvement Proposal | Continous Improvement Proposal System | Other Service |
| Document Management System | Document Management System | Other Service |


The remaining service list was then reduced to all services that reached more than 6 points. The remaining, 56 services, were taken into the 2nd iteration of service selection. For the corresponding service list after the 1st iteration see [HIFIS Cloud Service List after 1st Iteration](../Corresponding-files/HIFIS-Cloud_service-list-after-1st-iteration-of-service-selection.pdf)

## The 2nd Iteration of the service selection process

The 2nd Iteration of the service selection process started in May 2020 after the 1st iteration was completed. The centers with services that earned more than 6 points in the 1st iteration were asked to give us some more rough information on their services by filling out a prepared table (see [HIFIS Cloud - 2nd iteration information request to centers](../Corresponding-files/HIFIS-Cloud_2nd-iteration_Information-request-to-centers_GermanOnly.pdf)) which was delivered by E-Mail. Moreover, the centers which did not provide any of the “top” services were given the chance to give us some more information about their services, too. All this information was used to evaluate the following criteria (see [How Services are evaluated](how-services-are-evaluated.md) for details on how service selection criteria was made up) which were part of the 2nd iteration:

| # | Category | Criteria | Criteria Type | Unit | Weight | 4 | 3 | 2 | 1 | 0 |
| :- | :- | :- | :- | :- | :- | :- | :- | :- | :- | :- |
| 11 | Technical | Service uses no proprietary data formats/ interfaces (no vendor lock) | A | Yes/No | - | - | - | - | - | - |
| 12 | Technical | Service is capable to use Helmholtz AAI | A | Yes/No | - | - | - | - | - | - |
| 6 | Overhead | Service is ready get integrated into Helmholtz cloud |  B | Timeframe required to get service ready on Service Provider side | 0,81 | <4 wk | <2 mo | <6 mo | <1 yr | >1 yr |
| 14 | Technical | Service may be made available for external users | B | Availablity for external Users (**F**ull/**L**imited/**N**o) | 0,67 | F | - | L | - | N | 
| 17 | Technical | Service has a promising long-term perspective | B | Service's Longterm Perspective (**P**romising/**U**nknown/**T**errible) | 0,50 | P | - | U | - | T |

*Criteria Types: A - Exclusion criteria; B - Weighting criteria; I - Information*

The resulting maximum points a service could earn in the 2nd iteration were 7,92 points. The Helmholtz centers received the results of the 2nd iteration to verify them in comparison with the (same) services provided by other centers. 

There are no services from 2nd Iteration that were postponed to a future service portfolio.

The following 4 services from the 2nd iteration have no service provider (Status Quo: 05.08.2020):

| Service Type | Service | Service Category |
| ------ | ------ | ------ |
| Helmholtz hosted Webserver | Helmholtz hosted Webserver | Infrastructure Service |
| Software Catalog System | Software Catalog System | Community/Science Service |
| Data Management Plan | RDMO | Community/Science Service |
| Literature Management | Endnote (Cloud) | Collaboration Service |
 
Since a clear cut line to reduce the service list for the 3rd iteration could not be made up, we decided to take all services having a service provider from the 2nd iteration into the 3rd iteration (47 services). For the corresponding service list after the 2nd iteration see [HIFIS Cloud Service List after 2nd Iteration](../Corresponding-files/HIFIS-Cloud_service-list-after-2nd-iteration-of-service-selection.pdf).

## The 3rd Iteration of the service selection process

The 3rd Iteration of the service selection process started in August 2020 after the 2nd iteration was completed. A service canvas (see [HIFIS Cloud - Service Canvas](../Corresponding-files/HIFIS-Cloud_service-canvas.pdf)) was created in order to gain detailed information on each service left in the service list. The service canvas both gives the information necessary to evaluate the remaining service selection criteria and gives information that might be a part of the service catalogue at a later point of time. After having worked out the service canvas on a theoretical basis from both service portfolio as well as technical view, three example services were chosen as kind of a "proof of concept" to verify each field used in the service canvas. The chosen example services were chosen based on their maturity as well as on the characteric to be offered by multiple Helmholtz centers in the service list. Therefore, the example services were:

- JupyterHub (Jülich)
- GitLab (HZDR)
- Nextcloud (HZB)

In order to give the centers assistance to fill out the service canvas, six appointments to go through the service canvas field by field, explaining which information is requested, were offered to the centers from mid to end of August 2020. In combination with the practical examples named above the centers were ready to fill the service canvas with their service information and hand it back to us until the beginning of September 2020.

The following criteria (see [How Services are evaluated](how-services-are-evaluated.md) for details on how service selection criteria was made up) were part of the 3rd iteration:

| # | Category | Criteria | Criteria Type | Unit | Weight | 4 | 3 | 2 | 1 | 0 |
| :- | :- | :- | :- | :- | :- | :- | :- | :- | :- | :- |
| 10 | Technical | Service meets data protection and IT security requirements | A | Yes/No | - | - | - | - | - | - |
| 13 | Technical | Service supports user deprovisioning | A | Yes/No | - | - | - | - | - | - |
| 20 | Service Provider | Service provider must be able to provide the service under increased load of Helmholtz users (scalability) | A | Yes/No | - | - | - | - | - | - |
| 21 | Service Provider | Service provider has policies regarding the access to offered Helmholtz cloud services | A | Yes/No | - | - | - | - | - | - |
| 22 | Service Provider | Service provider established backup for service data (only applicable for services that store data) | A | Yes/No | - | - | - | - | - | - |
| 7 | Overhead | Service supports incubator interconnection (all platforms) | B | No. of incubator platforms supported | 0,38 | >1 | - | 1 | - | 0 |
| 15 | Technical | Service integration is easy to perform | B | Man weeks required in HIFIS for Service Integration | 0,13 | 0-3 | 4-6 | 7-9 | 10-12 | 12+ |
| 16 | Technical | User effort to enable the service is as low as possible | B | Service Enabling required by (**N**one, **U**ser, **A**dmin)| 0,25 | N | - | U | - | A |
| 18 | Technical | Service supports open access APIs etc. | I | Yes/No | - | - | - | - | - | - |
| 23 | Service Provider | Service providers are equally distributed among the Helmholtz centers | I | Yes/No | - | - | - | - | - | - |
| 24 | Service Provider | Service provider is certified | I | Yes/No | - | - | - | - | - | - |
| 25 | Service Provider | Service provider established processes to meet (legal) requirements concerning the deletion of data (data protection relevant)/ storage of data | I | - | - | - | - | - | - |

*Criteria Types: A - Exclusion criteria; B - Weighting criteria; I - Information*

The resulting maximum points a service could earn in the 3rd iteration were 3,04 points. Looking at the whole service selection process, a service could have earned a maximum of 18,68 points.

The following 8 services from the 3rd Iteration are postponed to a future service portfolio:

| Service Type | Service | Service Provider | Service Category |
| ------ | ------ | ------ | ------ | 
| On demand Storage | Storage (HDF) | KIT | Infrastructure Service |
| On demand compute (offering CPU and GPU ressources) | GPU compute Service | DESY | Infrastructure Service |
| On demand compute (offering CPU and GPU ressources) | OpenStack | DESY | Infrastructure Service |
| JupyterHub Service | JupyterHub | HZDR | Science/Community Service |
| Mailing list tool | Sympa | DESY | Collaboration Service |
| Project Management Wiki | OpenProject | HZB | Collaboration Service |
| Sync&Share with Cloud Office | Nextcloud (OnlyOffice) | HZDR | Collaboration Service |
| Event Management | Indico | DESY | Collaboration Service |

The following service from the 3rd iteration has no service provider (Status Quo: 07.09.2020):

| Service Type | Service | Service Category |
| ------ | ------ | ------ |
| On demand compute (offering CPU and GPU ressources) | Kubernetes Cluster | Infrastructure Service |

After all iterations, there were 38 Services left in the service list - see [HIFIS Cloud Service List after 3rd Iteration](../Corresponding-files/HIFIS-Cloud_service-list-after-3rd-iteration-of-service-selection.pdf). 

## Final results of service selection process

In the 3rd iteration, several services were excluded from the selection list of the Initial Service Portfolio. The service list was reduced by 10 services of which 

* 1 service has turned out to be part of another service,
* 1 service can not be offered by the service provider anymore, and 
* 8 services are postponed to a future service portfolio due to yet missing detail information for these services.

It was decided to take all 38 services left into the initial service portfolio and to integrate them into Helmholtz Cloud following the rank list in terms of points they earned in the service selection.

## Selected Services for Initial Helmholtz Cloud Service Portfolio

Reducing the services offered by multiple service providers to one list entry, the following 21 services are therefore part of the initial service portfolio:

| Service | Service Providers | Service Category
| ------ | ------ | ------ |
| OpenStack | Jülich, Jülich (HDF), KIT, DKFZ | Infrastructure Service
| Storage (HDF) | Jülich, DESY | Infrastructure Service
| HAICORE (HAICU, HIP) | KIT, Jülich | Infrastructure Service
| Singularity | KIT, Jülich | Infrastructure Service
| Docker | DESY | Infrastructure Service
| GPU Compute Service | Jülich, HZDR | Infrastructure Service
| AWI Marketplace | AWI | Infrastructure Service
| GitLab | HZDR, KIT, Jülich, Geomar | Science/Community Service
| JupyterHub | Jülich, DESY, DKFZ, HMGU | Science/Community Service
| B2Share (Invenio) | Jülich | Science/Community Service
| JupyterHub Notebooks on HPC | KIT | Science/Community Service
| ODV | AWI | Science/Community Service
| RODARE | HZDR | Science/Community Service
| Ocean and Climate Sensor Management | AWI | Science/Community Service
| Rocket.Chat | Jülich | Collaboration Service
| Zammad | HZDR | Collaboration Service
| Mattermost | HZDR | Collaboration Service
| Nextcloud (OnlyOffice) | KIT, HZB, DESY | Collaboration Service
| LimeSurvey | HMGU, DKFZ | Collaboration Service
| Redmine | HZDR, HMGU | Collaboration Service
| ShareLaTex | HZDR | Collaboration Service

For the detailed results of all three iterations of the service selection process in one list see [HIFIS Cloud Results of all three iterations of the service selection process - PDF](../Corresponding-files/HIFIS-Cloud_service-list-including-the-results-of-all-three-iterations-of-the-service-selection-pdf.pdf) or [HIFIS Cloud Results of all three iterations of the service selection process - Calculation sheet](../Corresponding-files/HIFIS-Cloud_service-list-including-the-results-of-all-three-iterations-of-the-service-selection-calculation-sheet.xlsx)

The key figures of the service selection/initial service portfolio can be found in the following summary table:

![Alt-Text](../Corresponding-files/HIFIS-Cloud_summarized-key-figures.png)

For a compact summary of the key figures regarding the service selection/initial service portfolio in PDF format, see [HIFIS Cloud - Summarized key figures on service selection/initial service portfolio](../Corresponding-files/HIFIS-Cloud_Initial-Service-Portfolio-summarized-key-figures.pdf)

## Next steps regarding Service Portfolio Management

The next steps for Service Portfolio Management as well as the next steps towards service integration can be checked on the HIFIS Website (corresponding link see [Roadmap for future Service Portfolios](https://hifis.net/roadmap/))

For further aspects on Service Portfolio Management, see [Further Information on Service Portfolio Management](../service-portfolio-management/further-information-on-service-portfolio-management.md)

(Status Quo: 01.10.2020)
