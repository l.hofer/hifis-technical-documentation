# How the service list was created
Copied from HIFIS Cloud Annual Report 2019, resulting service list see [HIFIS Cloud Service List after two service survey rounds](../Corresponding-files/HIFIS-Cloud_service-list-after-two-service-survey-rounds.pdf).

From June 2019 to April 2020, a service survey throughout 18 Helmholtz centers (+ the Helmholtz Head Office) has been conducted to collect the services that are desired to be offered in HIFIS (see table below for information on which centers was visited when). A second target is to identify which centers can act as service providers for the services offered in HIFIS. The survey is conducted in two steps: In the first round, two of the coordinators (Uwe Konrad from HZDR and Ants Finke from HZB) travelled to each center and determined specific needs and potential supplies in discussion with local IT representatives and, separately, scientific working groups. Based on the proposal, services were queried in the categories Infrastructure Services, Community Services, Scientist Services and Collaboration Services. With the completion of the interviews in January 2020, a second round of the survey has been conducted. In a consolidated list, the centers were able to check, adapt and amplify the information collected. The resulting verified service list was taken as a basis for service evaluation/selection.

## Helmholtz centers visited during the service survey (1st Round)

| Month | Centers visited |
| ------ | ------ |
| June 2019 | UFZ |
| July 2019 | Helmholtz Head Office, GFZ | 
| August 2019 | HZDR, HZI, KIT, DESY, HZB, GSI, Jülich | 
| September 2019 | HMGU, MDC | 
| October 2019 | DLR, DKFZ, HZG | 
| December 2019 | AWI, Geomar |
| January 2020 | DZNE, CISPA | 

