# Helmholtz Cloud - Service Portfolio

The following sites include:

## Creation of the initial Service Portfolio 
* [How the service list as a data basis for the service selection was created](initial-service-portfolio/how-the-service-list-was-created.md)
* [How the evaluation criteria used in the service selection were defined](initial-service-portfolio/how-services-are-evaluated.md)
* [How the service selection process was performed](initial-service-portfolio/how-services-are-selected.md), including the corresponding results

## Service Portfolio Management
* [Roadmap for future Service Portfolios](service-portfolio-management/roadmap-for-future-service-portfolios.md)
* [Further information on Service Portfolio Management](service-portfolio-management/further-information-on-service-portfolio-management.md)
* [Current Services in the Service Portfolio](service-portfolio-management/current-services-in-portfolio.md)
