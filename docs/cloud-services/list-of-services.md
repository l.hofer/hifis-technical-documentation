# Available Cloud Services

Here, the available Cloud Services with connection to the Helmholtz AAI are listed.
Depending on maturity and integration status, they separate into Helmholtz Cloud Services and (non-Helmholtz Cloud) Associated Services.

## Helmholtz Cloud Services

The full initial set of Helmholtz Cloud Services (which are also connected to the Helmholtz AAI) [has been defined end of 2020](https://hifis.net/news/2020/10/13/initial-service-portfolio.html).

Many of them are being integrated during 2021.
See also our [HIFIS Roadmap](https://hifis.net/roadmap/)
and the list of services in the [Service Portfolio Catalogue and Pipeline](../service-portfolio/service-portfolio-management/current-services-in-portfolio.md).

### Fully Integrated Services

Fully integrated services, comprising the collection of readily accessible services in the Helmholtz Cloud, are listed in the Helmholtz Cloud Portal at [**cloud.helmholtz.de** :fontawesome-solid-external-link-alt:](https://cloud.helmholtz.de/#/services).
There you can also find Service Level Commitments, specific usage conditions, helpdesk contacts, etc.

### Pilot Services

The following list comprises services that are scheduled to be integrated as full services soon.
That is, these services are part of the [Service Catalogue Pipeline](../service-portfolio/service-portfolio-management/current-services-in-portfolio.md).
For the time being, they are already preliminarily accessible as **Pilot Services**.

!!! info
    Note that usage conditions and technical details of these services may change without further notice until full integration is completes.
    Some services yet need manual steps for authentication and/or authorisation.

- [**dCache Storage Service**](https://dcache-demo.desy.de), provided by DESY.
  [Instructions for gaining access are provided here](pilot-services/prototype-dcache.md).
- [**RocketChat**](https://juchat.fz-juelich.de/) Messaging System, provided by FZJ.
- [**Rancher Managed Kubernetes Cluster**](https://rancher.desy.de), provided by DESY.
  So far, manual authorisation needed.
- [**LimeSurvey**](https://survey.hifis.dkfz.de), provided by DKFZ.
  [Instructions for access can be found here](../LimeSurvey_DKFZ/docs/).


## Associated Services (non Helmholtz Cloud)

More services are connected to the Helmholtz AAI, but do *not* belong to the Helmholtz Cloud, i.e. they are not included in the Helmholtz Cloud Service Onboarding process.
They are referred to as Associated Services.
Future application for Service Onboarding is possible and may be initiated by the service provider.

!!! warning "Please note:"
    No HIFIS support is supplied for Associated Services.
    HIFIS provides no information on any aspect of these services.
    Please contact the service provider directly in case you have any questions or usage requests.


### Exemplary Associated Services

This is an exemplary list of Associated Services that may be of use for end users.
This list is not complete or comprehensive.
If you'd like to propose changes, please [contact us :fontawesome-regular-envelope:](mailto:support@hifis.net).

- **Gitlab** DevOps lifecycle tool:
    - [provided by DESY](https://gitlab.desy.de/),
    - [provided by GFZ](https://git.gfz-potsdam.de/),
    - [provided by HZB](https://gitlab.helmholtz-berlin.de/users/sign_in).
- [**HedgeDog Notes**](https://notes.desy.de/) markdown writing and sharing platform.
  Open for All-Helmholtz and partners, provided by DESY.
- [**HIFIS Events Management**](https://events.hifis.net), based on CERN's Indico.
  Open for All-Helmholtz and partners, provided by DESY.
- [**Orpheus**](https://orpheus.data.kit.edu/providers/Helmholtz%20AAI) OpenID Connect investigation service, provided by KIT.
- [**SonarQube**](https://sonar.desy.de/) platform for continuous inspection of code quality, provided by DESY.

## Complete list of technically connected services to Helmholtz AAI

The complete list of connected services to the Helmholtz AAI - without any ordering or rating - can be found at:

* [All Services connected to the Production Unity](https://login.helmholtz.de/unitygw/VAADIN/files/connected-services.html)
* [All Services connected to the development instance for integration testing](https://login-dev.helmholtz.de/unitygw/VAADIN/files/connected-services.html)
