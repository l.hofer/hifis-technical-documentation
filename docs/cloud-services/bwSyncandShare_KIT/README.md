# bwSync&Share Documentation

The bwSync&Share service enables employees and students of Helmholtz institutions to store data on KIT storage systems in order to synchronize them between desktop computers and mobile devices. In addition, users can make their stored data accessible to other users by assigning appropriate access rights. In this context, access rights can also be granted to users who do not belong to the participating institution.

By integrating OnlyOffice, an online editor comparable to Google Docs, Office documents can be edited via the web interface even without installing an Office suite on the end device. In addition to synchronizing and sharing files, this also enables joint, simultaneous editing of files.

After a user has registered for this service, authentication and authorization for service use always takes place via the Helmholtz-AAI (full implementation in progress). The service can be used cross-platform via web browser, additionally clients for the operating systems Windows, Linux, Mac OS, as well as mobile apps for iOS and Android are available.


## Required Attributes
Currently, only users of Helmholtz centres whose Identity Providers (IdP) provide the correct entitlement can access this service. 
Access will only be possible to users of centres, which IdP transmits this entitlement.

* `urn:geant:helmholtz.de:kit:res:bwsyncandshare`

## Data protection and data security
Communication between the end devices and the central storage systems is encrypted. The data is stored in encrypted form on KIT storage systems and thus remains within the German legal area. Data access is restricted to the user who initially stored the data and to other users authorized by the data owner by granting access rights. There is no backup in the form of a second copy.

The service is not approved by the operator for the storage of personal data.
When registering for the bwSync&Share service, the following user information is transmitted from the home institution to the service provider KIT in encrypted form and stored there:

* name and surname
* email address
* home institution
* Unique user ID (EPPN & persistent ID)
* Status of the user (student, employee, guest)

With your consent, you agree that the above data may be transferred to KIT for the purpose  of authentication and service provision. If you do not participate in the bwSync&Share service, this data will not be transferred to the bwSync&Share service. The provisions of the State Data Protection Act (LDSG) of the state of Baden-Württemberg and sector-specific data protection regulations (in particular German Telecommunication Act and German Telemedia Act) in the respectively applicable versions are observed.
The » Regulations of Digital Information Processing and Communication« (I&C) at the Karlsruhe Institute of Technology (available at <https://www.stab.kit.edu/downloads/201310_IuK_Ordnung_engl.pdf>) apply.

## Operating time/service time/response time/availability
The systems of the service infrastructure are in regular operation around the clock. During the service time (weekdays from 09:00 to 17:00) the reaction time is four hours. The KIT strives for the highest possible service availability. Planned service interruptions (e.g. for maintenance work) are announced in advance with a period of five days.

## Costs
The use of bwSync&Share within the Helmholtz Cloud is free of charge for members of Helmholtz institutions.

## Storage
The amount of storage space available to a user is limited to 50 GByte.

## Tickets & Support
If you have any further questions, please contact the support team  of the institution to which you belong. There your request can be  recorded and if necessary forwarded to the support for  bwSync&Share. 

## More Links & Resources

* bwSync&Share Website: <https://bwsyncandshare.kit.edu>
* Hilfe & FAQ (deutsch): <https://help.bwsyncandshare.kit.edu/>
* Help & FAQ (english): <https://help.bwsyncandshare.kit.edu/english/index.php>


