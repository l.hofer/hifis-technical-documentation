# HDF-Cloud OpenStack Cluster @FZJ
--------------------------------

FZJ (Forschungszentrum Jülich) offers an OpenStack-Cluster for selected users to run Virtual Machines on the HDF-Cloud. More information about the cluster: [OpenStack@FZJ](https://www.fz-juelich.de/ias/jsc/EN/Expertise/SciCloudServices/HDFCloud/_node.html)

- **Authentication**: You can authenticate at the HDF-Cloud@FZJ with either your credentials of an existing HPC-account or through the HIFIS AAI instance Unity. The HIFIS AAI forwards a set of your credentials to the OpenStack dashboard. You will be prompted to accept the forwarding of your credentials.
- **Authorization**: only selected users are authorized to run virtual machines on the HDF-Cloud @FZJ. The list of users is managed on as a group in the HIFIS AAI.

![HDF-Cloud@JSC-group.png](images/HDF-Cloud@JSC-group.png)

## Accessing the service

The HDF-Cloud@FZJ is available at: [https://hdf-cloud.fz-juelich.de/](https://hdf-cloud.fz-juelich.de/)

In the selection box choose “Sign in with HDF-AAI”.
![HDF-Cloud@JSC-login1.png](images/HDF-Cloud@JSC-login1.png)

You will be redirected to the HDF-AAI development instance.
![HDF-Cloud@JSC-login2.png](images/HDF-Cloud@JSC-login2.png)

Choose your home IdP.
![HDF-Cloud@JSC-login3.png](images/HDF-Cloud@JSC-login3.png)

Login at your home IdP
![HDF-Cloud@JSC-login4.png](images/HDF-Cloud@JSC-login4.png)

and accept forwarding of your attributes to HDF-AAI.
![HDF-Cloud@JSC-login5.png](images/HDF-Cloud@JSC-login5.png)

Afterwards you will be redirected to the HDF-Cloud @FZJ and can configure a virtual machine.
![HDF-Cloud@JSC-login6.png](images/HDF-Cloud@JSC-login6.png)
