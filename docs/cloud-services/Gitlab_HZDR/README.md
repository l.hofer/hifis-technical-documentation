# HZDR Gitlab (Helmholtz Gitlab)

GitLab is a complete DevOps platform supporting Development, Security, and Ops teams to collaborate and build software. This GitLab service is running the Open Source version of GitLab. It allows you to:

* Host code in repositories with Git version control
* Implement project management with an Issue Tracker, Merge Requests or Issue Boards
* Use the provided shared runners for built-in Continuous Integration
* Host your Docker images using the Container Registry
* Share your documentation or static websites with Pages
* Share private or public software packages for a variety of common package managers, e.g. Conan (C++) or PyPI (Python).

Gitlab is provided by [HZDR](https://www.hzdr.de/).

### User Enablement
This GitLab service can be used free-of-charge by anybody within Helmholtz and their partners. Login via the Helmholtz AAI. Your account will automatically be provisioned.

Contact for user support: <support@hifis.net>.

## Technical Documentation 

* [AAI Integration](aai.md)
