# Welcome to HIFIS LimeSurvey Documentation

LimeSurvey is an online Survey Service offered by DKFZ for use to the members of the [Helmholtz Association](https://www.helmholtz.de) under Helmholtz AAI services.

## Contact

* For any help regarding LimeSurvey, write to [**Support**](mailto:support@hifis.net?subject=[lime-survey-dkfz] - Support Request).

## Mailing  List

* Await important notifications about maintainance activities, new features from our mailer  **hifis-limesurvey-dkfz-announce@dkfz-heidelberg.de**

To Subscribe [**Click Here**](mailto:hifis-limesurvey-dkfz-announce-join@dkfz-heidelberg.de?subject=Subscribe to mailing list)

And while we are sorry to see you go, to Unsubscribe  [**Click Here**](mailto:hifis-limesurvey-dkfz-announce-join@dkfz-heidelberg.de?subject=Unsubscribe from mailing list)


## FAQs

* **How to Sign-up for HIFIS LimeSurvey?** <br/>

Reach out to [**Support**](mailto:support@hifis.net?subject=[lime-survey-dkfz] - Sign-up Request). A Data Protection Agreement is to be set up with each partner institute.

* **How to create survey?** <br /> <br />
After login, on the top right corner of the page. <br /> 
![Placeholder](./pictures/select_create.png) <br /> 
<br /> 
<br />

Click on the arrow next to **Surveys** button. Select **Create a new survey**. <br />
![Placeholder](./pictures/create_survey.png) <br /> 
<br /> 
<br />

Give the desired name and fill out other relevant details before clicking on **SAVE**. <br />
![Placeholder](./pictures/entries.png) <br /> 
<br /> 
<br />

* **How to provide survey permission?** <br />

Go to list of surveys. <br /> <br />
![Placeholder](./pictures/list_of_survey.png) <br /> <br /> <br />

Click on the survey you wish to select. <br /> <br />
![Placeholder](./pictures/survey_selection.png) <br /> <br />
<br />

On the left scroll down to Survey Permissions. <br /> <br />
![Placeholder](./pictures/survey_permission.png) <br /> <br />
<br />

Click on the drop down for users to provide access to a particular user. <br /> <br />
![Placeholder](./pictures/user_list.png) <br /> <br />
<br />

Click on drop down for group to select which group should have access to the survey. <br /> <br />
![Placeholder](./pictures/group_list.png) <br /> 
<br />
<br />

* **How to publish a survey?** <br />

From the list of surveys, Select the desired survey. <br /> <br />
![Placeholder](./pictures/list_of_survey.png) <br /> 
<br /> 
<br />


Click on **Activate this survey** on the top menu. <br /> <br />
![Placeholder](./pictures/activate1.png) <br /> <br /> <br />

Verify entries and click on **Save & activate survey**. <br /> <br />
![Placeholder](./pictures/activate2.png) <br /> <br /> <br />

## Imprint

Herausgeber<br/>
Anbieter dieses Online-Umfragesystems ist das Deutsche Krebsforschungszentrum (DKFZ)


Anschrift<br/>
Deutsches Krebsforschungszentrum<br/>
Im Neuenheimer Feld 280<br/>
69120 Heidelberg<br/>
Telefon: +49 (0)6221 420<br/>
Internet: www.dkfz.de<br/>
E-Mail: kontakt@dkfz.de<br/>


Rechtsform<br/>
Das Deutsche Krebsforschungszentrum ist eine Stiftung des öffentlichen Rechts des Landes Baden-Württemberg. Gültig ist die Satzung vom August 2014.

Umsatzsteuer-Identifikationsnummer: DE 143293537


Vertretungsberechtigte Personen<br/>
Prof. Dr. Michael Baumann (Vorstandsvorsitzender und Wissenschaftlicher Vorstand)<br/>
Ursula Weyrich (kaufmännischer Vorstand)<br/>

(Anschriften wie oben)


System Verantwortlich<br/>
Die Information Technology Core Facility des Deutschen Krebsforschungszentrum.<br/>


Inhaltlich Verantwortlich<br/>
Für die Inhalte von einzelnen Umfragen ist der jeweilige Umfrage-Administrator verantwortlich.


## Privacy Policy

This policy is effective from 01.07.2021. <br/>

Privacy Policy

| Title                                     | Description           | 
| ----------------------------------------- | --------------------- | 
|1. Name of the Service                     | HIFIS LimeSurvey DKFZ |
|2. Descrption of the Service               | HIFIS LimeSurvey DKFZ is an online survey service <br/>
|                                           | offered by DKFZ for use to Helmholtz centres.  | 
|3. Data Controller and a contact person    | German Cancer Research Center - Foundation under 
|                                           | public law <br/>
|                                           | Im Neuenheimer Feld 280 <br/>
|                                           | 69120 Heidelberg <br/>
|                                           | Germany <br/>
|                                           | Telephone: +49 (0)6221 420 <br/>
|                                           | Email: kontakt@dkfz.de<br/>
|                                           | Website: www.dkfz.de| 
|4. Data Controller’s data protection       | Data Protection Officer <br/>
| officer (if applicable)                   | German Cancer Research Center - Foundation under 
|                                           | public law <br/>
|                                           | Im Neuenheimer Feld 280 <br/> 
|                                           | 69120 Heidelberg <br/>
|                                           | Telephone: +49 (0)6221 420 <br/> 
|                                           | Email: datenschutz@dkfz.de <br/>
|5. Jurisdiction and supervisory authority  | DE, Germany, Baden-Württemberg <br/>
|                                           | Notwithstanding any other administrative or <br/>
|                                           | judicial remedies, you also have the right <br/> 
|                                           | to lodge a complaint wit a supervisory authority, <br/>
|                                           |in particular in the Member State where you reside <br/>
|                                           | or work or where the alleged violation takes place <br/>
|                                           | if you believe that the processing of your personal <br/>
|                                           | data is in violation of GDPR. <br/>
|                                           |                            
|                                           | The supervisory authority to which the complaint is <br/>
|                                           | submitted informs the complainant on the status and <br/>
|                                           | the results of the complaint including the possibility <br/>
|                                           | of a judicial remedy as pursuant to Article 78 GDPR.|
|6. Personal data processed and the legal   | A. Personal data retrieved from your Home organisation: <br/>
| basis                                     | ● Pseudonymous identifier* <br/>
|                                           | ● Your Affiliation in your home organization, 
|                                           | based on eduperson schema* <br/>
|                                           | ● Assurance information of your home organization, 
|                                           | based on eduperson schema* <br/>
|                                           | ● Your Email Address <br/>
|                                           | ● Your full name, based on eduperson schema* <br/>
|                                           | B. Personal data gathered from yourself <br/>                             
|                                           | ●	Pseudonymous identifier <br/>
|                                           | ●	eduPerson Scoped Affiliation (1) (2) (3) <br/>
|                                           | ●	eduPerson Assurance (1) (2) (3) <br/>
|                                           | ●	Displayname <br/>
|                                           | ●	Email <br/> 
|                                           | ●	eduPerson Entitlement (1) (2) |
|7. Purpose of the processing of personal   |This data is necessary for account management purposes <br/> 
| data                                      | (e.g. to contact you to inform you of changes to the <br/>
|                                           | service or for security purposes), and for the reasons <br/>
|                                           | given in the paragraph below. Log records of your access <br/>
|                                           | to and actions on HIFIS LimeSurvey DKFZ are retained. <br/>
|                                           | These records contain: <br/>
|                                           | ● Pseudonymous identifier <br/>
|                                           | ● The network (IP) address from which you access HIFIS 
|                                           | LimeSurvey DKFZ <br/>
|                                           | ● Time and date of access <br/>
|                                           | ● Details of actions you perform <br/>
|                                           | This data is necessary to ensure that the HIFIS <br/>
|                                           | LimeSurvey DKFZ service is reliable and secure, <br/>
|                                           | such as for assisting in the analysis of reported+
|                                           | problems, contacting you if a problem is 
|                                           | identified with your account and <br/>
|                                           | responding to security incidents. This data may also be <br/> 
|                                           | used for authorised services acting on behalf 
|                                           | of authorised users. |
|8. Third parties to whom personal          | Your data will not be disclosed to third parties. |
|9. How to access, rectify and delete the   | Contact the contact personal above. To rectify the data <br/>
| personal data and object to its processing| released by your Home Organisation, contact your  
|                                           | Home Organisation’s IT helpdesk.|
|10. Withdrawal of consent                  | You have the right to revoke your consent to data <br/>
|                                           | processing at any time. Upon revoking consent, the <br/> 
|                                           | legality of the data processing carried out on the  
|                                           | basis of the consent will not be affected by the <br/> 
|                                           | revocation of consent. This can be done by contacting 
|                                           | support@hifis.net |
|11. Data portability                       | You have the right to receive the personal data <br/>
|                                           | concerning you which you have provided to a controller, <br/>
|                                           | in a structured, commonly used and machine-readable 
|                                           | format. Moreover, you also have the <br/> 
|                                           | right to transmit those data to another controller  
|                                           | without hindrance from the controller to <br/> 
|                                           | which the personal data have been provided in sofar as <br/>
|                                           | (1) the processing is based on a granted consent as <br/>
|                                           | pursuant to Article 6 (1) lit. a) GDPR or Article 9  
|                                           | (2) lit. a) GDPR or on a contract pursuant to <br/> 
|                                           | Article 6 (1) lit. b) GDPR and <br/>
|                                           | (2) the processing is carried out by automated means. <br/>
|                                           | In exercising this right, you also have the right to have <br/> 
|                                           | the personal data concerning you transmitted directly from <br/> 
|                                           | one controller to another, wherever technically feasible. 
|                                           | This may not adversely affect the rights and <br/>
|                                           | freedoms of others. <br/>
|                                           | The right to data portability does not apply to the <br/> 
|                                           | processing of personal data if it is required for the 
|                                           | performance of a task carried out in the <br/>
|                                           | public interest or in the exercise of official authority <br/> 
|                                           | vested in the controller. |
|12. Data retention                         | Personal data is deleted on request of the user or if the <br/>
|                                           | user hasn't used the service for 6 months |
|13. Data Protection Code of Conduct        | Your personal data will be protected according to the <br/> 
|                                           | Code of Conduct for Service Providers [1], a common <br/> 
|                                           | standard for the research and higher education sector
|                                           | to protect your privacy. | 


REFERENCES <br/>

[1] GÉANT Data Protection Code of Conduct - http://www.geant.net/uri/dataprotection-code-of-conduct/v1
