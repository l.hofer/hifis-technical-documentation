# Helmholtz Cloud Services (pilot)
The section contains documentation about pilot Helmholtz Cloud Services.
The launch of the productive version of Helmholtz Cloud and thus Helmholtz Cloud Services is envisioned for the second half of 2021.

The official access portal (currently beta) for these services can be found at <https://cloud.helmholtz.de>.

#### Available documentation on Services
* [**bwSync&Share**](bwSyncandShare_KIT/README.md) (provided by KIT)
* [**Gitlab**](Gitlab_HZDR/README.md) (provided by HZDR)
* [**LimeSurvey**](LimeSurvey_DKFZ/docs/index.md) (provided by DKFZ)
* [**Nubes**](Nubes_HZB/README.md) (provided by HZB)

More details on services will be published soon.

## Associated Services

See [here](../service-integration/pilot-services/pilot-services/) for a list of associated and other AAI connected services.
