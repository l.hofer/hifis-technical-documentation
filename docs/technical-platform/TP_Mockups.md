# HIFIS Technical Platform - Mockups

!!! warning
    Note that this documentation is outdated.

[TOC levels=2-4]: # "## Table of Contents"



## Table of Contents
- [Document History](#document-history)
- [Scope](#scope)
- [General](#general)
- [Mockups](#mockups)
  - [Login](#login)
  - [Initial List View](#initial-list-view)
  - [Initial List View With Categories](#initial-list-view-with-categories)
  - [Detail View](#detail-view)


## Document History

Current Version: 0.1

Current Version Date: 2020-05-04

## Scope

This document is intended to contain first UI mockups as a base for the design of the technical platform (TP) within HIFIS.

## General

The mockups provide a method to give a first idea, how the user interface can look like. They mainly are a mean to support further discussions about possibilities to develop the final design.

## Mockups

### Login

![Login](images/cloud-tp-mockups-login.png)

<a name="img-cloud-tp-mockups-login">**Figure 1:** Login</a>

### Initial List View

![Inital list view](images/cloud-tp-mockups-starting page (list).png)

<a name="img-cloud-tp-mockups-starting-page-list">**Figure 2:** Inital list view</a>

### Initial List View With Categories

![Inital list view with categories](images/cloud-tp-mockups-starting page (categories).png)

<a name="img-cloud-tp-mockups-starting-page-categories">**Figure 3:** Inital list view with categories</a>

### Detail View

![Detail view](images/cloud-tp-mockups-detail view.png)

<a name="img-cloud-tp-mockups-detail-view">**Figure 4:** Detail view</a>
