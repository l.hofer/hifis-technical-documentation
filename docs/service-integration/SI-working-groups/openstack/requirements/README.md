# Service Integration - OpenStack - Requirements overview

Requirements overview for service **OpenStack**

Content:

* [Licensing](licensing.md)
* [Software](software.md)
* [Technical-Platform](tp/)
