# Service Integration - Kubernetes - Requirements overview

Requirements overview for service **Kubernetes**

Content:

* [Licensing](licensing/)
* [Software](software/)
* [Technical-Platform](tp/)
