# Service Integration - Jupyter - Requirements overview

Requirements overview for service **Jupyter**

Content:

* [Licensing](licensing.md)
* [Software](software.md)
* [Technical-Platform](tp/)
