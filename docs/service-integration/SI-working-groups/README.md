# Service Integration - Working groups

Documentation for working group Service Integration (SI) in HIFIS.

There are working groups for the following services / softwares:

* GitLab
* [Jupyter](jupyter/)
* [Kubernetes](kubernetes/)
* [Nextcloud](nextcloud/)
* [OpenStack](openstack/)
