# Service Integration - Nextcloud - Technical-Platform overview

Technical-Platform overview for service **Nextcloud**

Content:

* [Accounting](accounting.md)
* [META-Data](metadata.md)
* [Monitoring](monitoring.md)