# Service Integration - Nextcloud - META-Data

META-Data for service **Nextcloud**

### Needed by TP from SI

* user capacity
* storage capacity
* API url

### Needed by SI from TP

* username
* first name
* last name
* mail address
* center(?)
* capacity
