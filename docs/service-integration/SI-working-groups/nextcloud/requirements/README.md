# Service Integration - Nextcloud - Requirements overview

Requirements overview for service **Nextcloud**

Content:

* [Licensing](licensing.md)
* [Software](software.md)
* [Technical-Platform](tp/)
