# Helmholtz Cloud Agent

In the Helmholtz Cloud, various heterogenous services are connected to the central [cloud portal](https://cloud.helmholtz.de).
While the cloud portal itself provides a well described ReST API,
at least some of the services are located in an internal network,
providing only limited access and/or functionality for the user and account management.
In this case, there is need for a proxy which acts as mediator between the cloud portal and the service / service provider.

## Solution: The Helmholtz Cloud Agent

The main tasks of the **Helmholtz Cloud Agent (HCA)** is the handling of accounts and some functionality to provide monitoring / reporting to the cloud portal or other future components of HIFIS.

## Architecture and Implementation

!!! info
    The Helmholtz Cloud Agent and the overall architecture are currently under development.
    All specifications can change.

The currently envisioned over architecture is depicted [here](../#architecture).

### Repositories

* The sources for the Helmholtz Cloud Agent can be found in
  <https://gitlab.hzdr.de/helmholtz-cloud-portal/local-agent/core>
* The source for the Helmholtz Cloud Portal are located in
  <https://gitlab.hzdr.de/hifis/cloud/access-layer/portal>


### Exemplary Use Case: nubes

First specific implementations of use cases are currently being implemented for the
[HZB nubes service](https://cloud.helmholtz.de/services?serviceDetails=nubes),
and the DESY hosted [cloud portal](https://cloud.helmholtz.de).
The principle components are depicted as follows:

![HCA_nubes_case](graphs/HCA_in_Helmholtz_PN.svg)

## Questions? Comment?

Don't hesitate to contact us at <support@hifis.net> if you have any questions  or comments on Helmholtz Cloud Agent.
