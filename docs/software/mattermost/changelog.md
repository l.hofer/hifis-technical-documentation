---
title: Service Changelog
---

## 2021-08-26

* Update Mattermost to version 5.37 -
  [Release Post](https://mattermost.com/blog/mattermost-v5-37/)
    * Try out [Collapsed Reply Threads](https://mattermost.com/blog/collapsed-reply-threads-beta/)
    * Enable yourself in **Account Settings > Display > Collapsed Reply Threads (Beta)**.
