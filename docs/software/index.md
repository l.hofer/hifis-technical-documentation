---
title: Service Documentation - HIFIS Software
---

# Service Documentation - HIFIS Software

HIFIS Software operates services that are available to members of the
Helmholtz Association and their guests.
On these pages we would like to answer and document service-specific questions.
This should be a useful addition to the general documentation of the services.

## Services

Currently, services marked with a green tick are available for use.
Services that are definitely planned to be made available in the future will
be added to the list with a grayed icon.

* [x] [Helmholtz Codebase aka GitLab](https://gitlab.hzdr.de) (part of [Helmholtz Cloud][cloud] Beta)
    * [x] Continuous Integration via GitLab CI
    * [x] Container and Package Registry
    * [x] Pages
* [x] [Mattermost](https://mattermost.hzdr.de) (part of [Helmholtz Cloud][cloud] Beta)
    * [x] Matterpoll plugin
    * [ ] GitLab plugin
    * [ ] Welcome Bot plugin
    * [ ] GitHub plugin
* [x] [Kroki](https://kroki.hzdr.de)
* [x] [HIFIS Helpdesk](https://support.hifis.net)
* [x] [Helmholtz AI Voucher Backend](https://zammad-voucher.helmholtz.ai/)

## Changelog

We document user-visible changes and improvements of the services on the
Changelog page.

* [GitLab](gitlab/changelog.md)
* [Mattermost Changelog](mattermost/changelog.md)

## Reproducible Service Administration via Ansible

All services are transparently deployed via so-called public
[Ansible roles](https://galaxy.ansible.com/hifis/)
and Helmholtz internal accessible
[Ansible-managed deployment projects](https://gitlab.hzdr.de/hifis-software-deployment/):

* [GitLab](https://gitlab.hzdr.de/hifis-software-deployment/gitlab)
* [Mattermost](https://gitlab.hzdr.de/hifis-software-deployment/mattermost)
* [Kroki](https://gitlab.hzdr.de/hifis-software-deployment/kroki)
* [HIFIS Helpdesk](https://gitlab.hzdr.de/hifis-software-deployment/helpdesk)

## Provided to you as part of HIFIS and [Helmholtz Cloud :fontawesome-solid-external-link-alt:][cloud]

![LOGO HIFIS Software](../images/hifis_logo.svg)

**The services are administered by:**

<div style="display: grid;grid-template-columns: 1fr 1fr;justify-items: center;align-items: center;">
    <img src="../images/hzdr_logo.svg">
    <img src="../images/ufz_logo.png">
</div>

As of today all services presented in this documentation are operated entirely
in the [HZDR](https://www.hzdr.de) data center.
They are administrated by colleagues from the
[HIFIS Software](https://hifis.net/services#software) team at
[HZDR](https://www.hzdr.de) and [UFZ](https://www.ufz.de).

[cloud]: https://cloud.helmholtz.de/
