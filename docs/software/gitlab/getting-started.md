---
title: Getting Started
---

# Getting Started

The [Helmholtz Codebase Service, based on GitLab :fontawesome-solid-external-link-alt:](https://gitlab.hzdr.de)
can be used free-of-charge by anybody within Helmholtz and their partners.
It is hosted by HIFIS Software entirely on HZDR servers,
jointly created by [UFZ :fontawesome-solid-external-link-alt:](https://www.ufz.de) and [HZDR :fontawesome-solid-external-link-alt:](https://www.hzdr.de).
With the general availability of the Helmholtz Cloud,
the service will be available as a fully integrated Hemholtz Cloud Service
and will follow the given regulations.

## Sign-Up

### Available Options

Multiple options exist to sign-in the Helmholtz coding platform.

* **Helmholtz AAI:** This is the recommended login method for everyone except
  HZDR employees.
  Please note, that the Helmholtz AAI also supports the social providers
  _Google_, _GitHub_ and _Orcid_. 
* **HZDR LDAP:** :fontawesome-solid-exclamation-triangle: Only HZDR employees can use this method.
* **GitHub (Deprecated):** This method is still available for historical reasons.
  It is recommended to use the Helmholtz AAI instead.

### Helmholtz vs. Non-Helmholtz Users

Collaboration is a key component in software development.
This is why it is most important to enable anybody to easily contribute
changes to projects regardless of their affiliation.
Nevertheless, it is clear that we need to pose certain differences with respect
to users that do not belong to a Helmholtz organization.

* **Helmholtz employees** with registered IdP in Helmholtz AAI get full access to all components.
* **All other users** will become
  [external users :fontawesome-solid-external-link-alt:](https://gitlab.hzdr.de/help/user/permissions.html#external-users)
  and need to be invited to projects.
  This means they cannot create projects or groups.
  Nevertheless, they can be invited to any group or project and will have
  access according to the permissions of the
  [role :fontawesome-solid-external-link-alt:](https://gitlab.hzdr.de/help/user/permissions.html#project-members-permissions){ .md-badge .md-badge--primary }
  you assign.

## First Steps

Follow the GitLab [basic guides :fontawesome-solid-external-link-alt:](https://gitlab.hzdr.de/help/gitlab-basics/index.md)
to learn how to get started with the platform.
For more in-depth questions consider contacting the
[HIFIS Software Consulting :fontawesome-solid-external-link-alt:](https://hifis.net/services/software/consulting) team.

### Clone a Git Repository as a Helmholtz AAI User

GitLab, like all other Helmholtz AAI connected services,
does not recognize a password from users who log in via Helmholtz AAI.
Therefore, you cannot use your center password when Git asks for the
credentials while running the `git clone` command.
You have two options to interact with internal or private projects via `git`.

#### Create a Personal Access Token

* Go to <https://gitlab.hzdr.de/-/profile/personal_access_tokens> and create a
personal access token.
* Use the scopes `read_repository` and `write_repository`.
* (Optionally, define an expiration date after which the token is considered
  to be invalid.)
* Click on `Create personal access token`.
* GitLab will display the token once at the top of the page.
* Copy the token and save it in the password manager of your choice.

When Git asks for the password you can now enter the Personal Access Token you
just created and interact with Git as usual.

??? note "Screenshot of the Personal Access Token Page"
    ![Screenshot of the Personal Access Token creation](screenshots/access_token.png)

!!! info
    More information is available in the
    [GitLab documentation :fontawesome-solid-external-link-alt:](https://gitlab.hzdr.de/help/user/profile/personal_access_tokens.html).

#### Use SSH to Interact With Your Repositories

Setting up SSH once might be the more convenient option for interacting with
your repository.
In this case you do not need to supply your username and password each time.
For configuring your local Git installation to properly use SSH we invite you
to have a look into the GitLab documentation.
This guide will help you through the process of setting up Git and SSH.

The process looks like this:

1. [See if you have an existing SSH key pair. :fontawesome-solid-external-link-alt:][existing-ssh]
2. [Generate an SSH keypair if not already present. :fontawesome-solid-external-link-alt:][create-ssh]
3. [Add an SSH public key to your GitLab account. :fontawesome-solid-external-link-alt:][add-ssh]
4. [Verify that you can connect. :fontawesome-solid-external-link-alt:][verify-ssh]

!!! info
    * Setup Git and SSH: <https://gitlab.hzdr.de/help/ssh/index.md>
    * On Windows: Please additionally refer to
      <https://gitlab.hzdr.de/help/ssh/index.md#use-ssh-on-microsoft-windows>

[existing-ssh]: https://gitlab.hzdr.de/help/ssh/index.md#see-if-you-have-an-existing-ssh-key-pair
[create-ssh]: https://gitlab.hzdr.de/help/ssh/index.md#generate-an-ssh-key-pair
[add-ssh]: https://gitlab.hzdr.de/help/ssh/index.md#add-an-ssh-key-to-your-gitlab-account
[verify-ssh]: https://gitlab.hzdr.de/help/ssh/index.md#verify-that-you-can-connect
