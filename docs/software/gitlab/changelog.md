---
title: Changelog
---

## 2021-10-05

* Update GitLab to version 14.3
    *  Introduces further improvements in the
       [wiki editor](https://about.gitlab.com/releases/2021/09/22/gitlab-14-3-released/#edit-a-tables-structure-visually-in-the-new-wiki-editor)
    * Allows to
      [use variables in other variables](https://about.gitlab.com/releases/2021/09/22/gitlab-14-3-released/#use-variables-in-other-variables)
      in `gitlab-ci.yml`
    * More details are available in the
      [Release Post](https://about.gitlab.com/releases/2021/09/22/gitlab-14-3-released/)

## 2021-08-27

* Update GitLab to version 14.2
    * Introduces [stageless pipelines](https://about.gitlab.com/releases/2021/08/22/gitlab-14-2-released/#stageless-pipelines).
    * Adds a [Markdown live preview](https://about.gitlab.com/releases/2021/08/22/gitlab-14-2-released/#preview-markdown-live-while-editing)
      to the Web IDE and single file editor to reduce the need for context switches.
    * More details are available in the
      [Release Post](https://about.gitlab.com/releases/2021/08/22/gitlab-14-2-released/)

## 2021-08-09

* Update GitLab to version 14.1 -
  [Release Post](https://about.gitlab.com/releases/2021/07/22/gitlab-14-1-released/)
