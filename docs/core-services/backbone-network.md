# Helmholtz Backbone Network
The networks of the individual Helmholtz centers are being interconnected on the basis of a __high bandwidth__ network with __mutual trust__ and increased overall __security__.

The backbone is an overlay of the __DFN X-WiN__, using the existing connections Helmholtz centres have via DFN. It is a virtual local area network, or VLAN, that is orchestrated by DFN in its so-called "Helmoltz [VRF](https://en.wikipedia.org/wiki/Virtual_routing_and_forwarding)", specific to HIFIS, with no link to the internet.

The following map shows the DFN glass fiber network and the current centres connected to the Helmholtz Backbone.
![Map of the Helmholtz centres connected to X-Win and the additional backbone network](../images/DFN-and-Helmholtz-backbone.png)  
_Background map of Helmholtz centres taken from [helmholtz.de](https://www.helmholtz.de/ueber-uns/helmholtz-zentren/)._

Why do we need the backbone?

1. __Availability__ of resources: lower latency between centres.
2. __Protection__ of existing resources, for example shielding a sensitive resource in a Helmholtz centre from public HTTP requests.
3. __Simplified access__, for example by-passing firewalls for connections between Helmholtz centres.

### What are the use cases?
Two use cases are being developed in the frame of the Helmholtz Backbone:

- Use case 1: __Direct connection__ between private IP addresses of two different Helmholtz centres.

    This is typically a use case when a scientist is working in a satellite station of his institution in another centre and wants to access his home institution's servers.
    This use case is currently being implemented to connect __HZDR equipment at XFEL__ (via DESY) to the HZDR networks which are not normally accessible outside of HZDR's local network.
<br>
<br>

- Use case 2: __Data transfers__ using [WebFTS](https://webfts.cern.ch/) over the backbone.

    This use case is currently being investigated for sharing data between centres when the data itself should not be transferred through the internet. Like this, the transfers are conducted over a route that provides even higher security in addition to using a standard HTTPS connection. For more details on the transfer service provided by HIFIS, please visit [this page](fts-endpoint.md).

### Technical preparations
##### Centres with pre-existing [BGP](https://en.wikipedia.org/wiki/Border_Gateway_Protocol) peering with DFN
Each centre has to configure its routing to the Backbone, and may decide to use dedicated hardware or not. Especially, if a trust relationship between the HIFIS partners can be established, the firewall/IPS system between LAN and router via the "DFN Helmholtz VRF" could be omitted to allow faster data transfers, because the traffic has not to be deeply inspected. This is illustrated in the picture below for Helmholtz centre A.

```kroki-blockdiag
blockdiag {

  // set defaults
  default_fontsize = 22;

  // define classes
  class router [label = "Router"];
  class firewall [shape = diamond, height = 60, label = "Fire-\nwall", fontsize = 16];
  class dmz [shape = roundedbox, label = "DMZ"];
  class lan [label = "LAN"];
  class transparent [label = "", style = "none", shape = "none"];

  // define routes
  // Internet routes
  DMZ-A -- firewall-A -- router-A -- Internet;
  firewall-A -- LAN-A1;
  Internet -- router-B -- firewall-B1 -- DMZ-B;
  firewall-B1 -- LAN-B;

  // backbone routes
  LAN-A2 -- placeholder-2 -- router-A2 -- DFN-Helmholtz-VRF;
  DFN-Helmholtz-VRF -- router-B;

  // define groups
  group HZ1 {
    label = "Helmholtz centre A";
    color = "#27b4f8";
    DMZ-A, LAN-A2, router-A2, router-A, placeholder-2;
    group {
      orientation = portrait;
      color = "#27b4f8";
      LAN-A1, firewall-A;
    }
  }

  group VRF {
    shape = line;
    style = "none";
    Internet, placeholder, DFN-Helmholtz-VRF;
  }

  group HZ2 {
    label = "Helmholtz centre B";
    color = "#8CB423"
    router-B, DMZ-B;
    group {
      orientation = portrait;
      color = "#8CB423"
      firewall-B1, LAN-B;
    }
  }

  // set shapes and classes
  Internet [shape = cloud, width = 200, height = 100];
  DFN-Helmholtz-VRF [shape = ellipse, width = 200, height = 100, label = "DFN\nHelmholtz VRF"];
  router-A, router-A2, router-B [class = router];
  firewall-A, firewall-B1 [class = firewall];
  DMZ-A, DMZ-B [class = dmz];
  LAN-A1, LAN-A2, LAN-B [class = lan];
  placeholder, placeholder-2 [class = transparent];
}
```

##### Centres without existing BGP peering with DFN
Some centres don't have an existing BGP peering with DFN. In this case, it is also possible to set a routing encapsulation or "[GRE](https://en.wikipedia.org/wiki/Generic_Routing_Encapsulation) tunnel" from the Helmholtz institute to (e.g.) DESY and within this GRE tunnel configure a BGP peering. This is for example the case for UFZ which is currently connected to the Backbone via DESY.

### Drafts of policies (restricted to HIFIS)

- [Helmholtz backbone policies draft](https://nubes.helmholtz-berlin.de/f/497309313)

### Monitoring

!!! info
    Upcoming: Proof of concept and testing for possible hardware and software issues.

### Connected Helmholtz Centres (restricted to HIFIS)

- [Helmholtz backbone IP announces and other relevant information per centre](https://nubes.helmholtz-berlin.de/f/322731997)
