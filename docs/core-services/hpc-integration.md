# Using 2FA and securely managed SSH key to access resources

KIT developed a solution that allows using a second factor authentication (2FA) in the combination with SSH Keys.

The following image shows the currently realized federated login to the KIT HPC systems, where SP-IdP identity and infrastructure proxy are integrated in one system, called reg-app. 
The reg-app system offers a service portal, where (DFN-AAI authenticated) users can view a list of all available services and register for them. Depending on the service, a service password has to be set and SSH keys with different usage profiles (login vs. remote command execution) can be uploaded. 
A connected LinOTP service is used to check second factor codes and to allow users to register TOTP-based smartphone authenticators through the portal. 
To enable web-based and commandline authentications, SAML, OIDC, LDAP and some customized HTTP-REST endpoints are available. For web-based logins, the SAML and OIDC endpoints are used. 
The LDAP endpoint is used to check service passwords, the REST endpoints are used to query a check for the 2nd factor code and to download the user's SSH keys.

With these building blocks, any service can be connected to the DFN-AAI user federation in a secure state of the art way. 
On modern systems, this can be done without the need to use customized ore even patched PAM or other authentication modules, it's just configuration of standard components.

![KIT_HCP.eng.png](images/KIT_HCP.eng.png)

In detail, the initial registration process at the SP-IdP proxy service portal is done as follows:

1. First of all, the user has to be added to an HPC project group.
2. The user authenticates at the SP-IdP-Proxy using a redirected home IdP login via SAML.
3. Beeing redirected back to the SP-IdP-Proxy, the user registers for the HPC service.
4. If not already done, the user configures a second factor (depending on the user's origin).
5. Finally the user sets a service password and uploads an SSH public key.

The login to a compute ressource is the done as follows:

1. User logs in via an interactive SSH shell, entering username, 2nd factor code and service password. These data is checked by the SSH server agaist the infrastructure proxy's LDAP endpoint. The 2nd factor is checked using an HTTP call to the SP-IdP-Proxy, which handles the associated secrets (internally or backed by a LinOTP service, depending on the users origin).
2. If these checks are successful, the uploaded SSH public key is unlocked at the infrastrucure proxy.
3. For the next hour an SSH key based login is possible. The SSH public key check is temporarily HTTP-downloaded by the SSH service from the infrastructure proxy, but not persistent on the HPC filesystem.
4. At every login, the SP-IdP-Proxy does an attribute query to the user's home IDP to ensure the account is already active and not deprovisioned.

There is also a web-based Jupyter hub access to the HPC resources realised:

1. The user logs in to Jupyter hub with a web browser, using an OpenID Connect redirect to the SP-IdP-Proxy, which redirects to the user's home IDP using SAML.
2. The user authenticates at the home IDP and gets redirected to the SP-IdP-Proxy.
3. After that, the SP-IdP-Proxy querys for the second factor code, and if that is validated, the user is redirected to the Jupyter hub.

The following image shows how this HPC SSH/Jupyter solution integrates within the Helmholtz AAI infrastructure:

![KIT_HCP.eng.png](images/HIFIS_AAI_HPC_simplified.eng.png)