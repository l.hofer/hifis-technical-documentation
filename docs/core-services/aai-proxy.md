# Proxy for securing a backend via HelmholtzAAI

This README and the corresponding files can be found at [HZDR-gitlab](https://gitlab.hzdr.de/hifis/apache-oidc-proxy)

In the repo, a sample configuration for an Apache2 server acting as a reverse proxy with OIDC authentication for an unsecured backend application is stored.
This setup has been tested with the Helmholtz AAI.

## Prerequisites

a)

You need to register a client that acts as a relying party (RP) with the helmholtzAAI at [https://login.helmholtz.de](https://login.helmholtz.de). Click in the upper right corner of the page onto "No Account? Sign up.", then "Oauth2/OIDC client registration".
There, you need to fill all the form fields. Small help from our internal documentation:

1. User Name is the OIDC `client_id` (you can choose it, keep it somewhere safe, you will need it for your client!) 
2. Password is the OIDC `client_secret` (you can choose it, keep it somewhere safe, you will need it for your client!) 
3. Email Address is an email address for contacting the admin of the service 
4. Service Security Contact is the security responsible of the service. This may be additional people, for example in a hosted VM setup 
5. Site Security Contactis your computer centre security contact. Typically your CERT. 
6. Service DPS URL: This is your Data Privacy Statement (DPS). Required by law. Find a DPS template here

The well_known configuration of login.helmholtz.de is here:
`https://login.helmholtz.de/oauth2/.well-known/openid-configuration`
You need to fill those parameters in the `auth_openidc.conf` for your client.

b)

You will need an SSL certificate and private key for the proxy server, copy them to a secure location on the server (use `chmod 600 private.key` to secure the private key against unauthorized access on the file system) and put the path in `default-ssl.conf` at `SSLCertificateFile` and `SSLCertificateKeyFile`. This is needed for a secure connection between your server, the client and the HelmholtzAAI. You can choose to get a certificate from your own CERT or via Let's Encrypt. Please seek advice from your local IT if you are unsure how to do any of this.

c)

Installation of the Apache:

1. Ubuntu: `sudo apt-get install apache2 apache2-dev apache2-bin libapache2-mod-auth-openidc`. The mod_auth_openidc module is not included in the standard distribution of apache and thus needs to be specified on top.
2. CentOS: `sudo yum install httpd`. If the mod_auth_openidc module is not included here, have a look at the binary package releases on [Github](https://github.com/zmartzone/mod_auth_openidc/releases).

# Proxy description and setup
It is assumed that the backend service (another http server or application) is not able to take care of OIDC-based authentication by itself. Therefore, the backend should be isolated from the outside by firewall rules and only be accessible by the proxy's IP and/or other trusted IPs.

The proxy is set up in a way that it redirects from the main URL to the unsecured parts of your backend app and only needs authentication once you enter paths that start with "/secure".
Upon clicking any link leading to that path, the user is redirected to the login page of the OIDC-provider (OP) for login there and can afterwards access the secured paths of the backend application because the OP sets a cookie in the user's browser.

**Caveat**: The secured location will be a prefix in the backend's paths, e.g. https://your.proxy.domain.de/secure/location/in/backend. Keep that in mind for the backend application and use relative links there together with the html tag `<base href="https://your.proxy.domain.de/secure/" />` in the `<head></head>` section of each html page. Including this `<base />` directive in any route and adjusting the links leading there will place it in the secured path.

The Apache server is modular and for certain tasks, modules have to be configured and loaded in addition to the Apache core.
In the config files provided with these instructions, the site- and client-specific parameters that need to be filled in before enabling the corresponding module/config are written in UPPERCASE letters.
The config files provided have to be copied into the respective directories (sites-available, mods-available) in /etc/apache2 and be activated with a2enmod / a2ensite:

1. The `openidc.conf` file goes into `mods-available` and is activated with `a2enmod auth_openidc.conf` while the corresponding module is activated via `a2enmod auth_openidc.load`.
2. The two files `000-default.conf` and `default-ssl.conf` belong into `sites-available` and are activated with `a2ensite 000-default.conf` and `a2ensite default-ssl.conf`.
3. For SSL/TLS connections (HTTPS) to work you have to enable the ssl module with `a2enmod ssl`.
4. The proxy functionality is activated by loading the proxy modules with `a2enmod proxy` and `a2enmod proxy_http`.
