# Documentation for the HIFIS Cloud and AAI

This repository contains the documentation for all aspects of the HIFIS
cloud and Backbone AAI. 

The documentation is deployed to and publically available at
**<https://hifis.net/doc/>**.

## Conventions

Markdown is the common format for the documentation. For the general syntax see <https://www.markdownguide.org/basic-syntax>.

## Contribute

You can contribute by logging in via Helmholtz AAI, create a branch from master, do you suggested changes and invite a project maintainer to review (and possibly merge) your changes.

## Local builds, External Resources

This documentation repository uses submodules.
For local builds, clone this repository *recursively*, with:
```
git clone --recursive git@gitlab.hzdr.de:hifis/overall/hifis-technical-documentation.git
```

If you have cloned the repository already, you might need to update the submodules with
```
git submodule sync --recursive
git submodule update --remote --recursive
```

This documentation uses the following plugins.
For local builds, you need to install them as necessary:

* Kroki diagrams: See [introduction here](https://hifis.net/tutorial/2021/06/02/easy-diagram-creation-in-gitlab).   
    `pip install mkdocs-kroki-plugin`.
* The [redirect plugin](https://pypi.org/project/mkdocs-redirects/)  
  `pip install mkdocs-redirects`
* The [material theme](https://github.com/squidfunk/mkdocs-material)  
  `pip install mkdocs-material`

The repository is public readable and it is writable for Helmholtz AAI logged in persons.


## Automatic Builds (CI)

* Master build review app (password required):  
  <https://hifis-review-app.hzdr.de/review-apps/hifis-hifis-technical-documentation/master/>
* Download latest master build:  
  <https://gitlab.hzdr.de/hifis/hifis-technical-documentation/-/jobs/artifacts/master/download?job=build>
* Browse latest master build:  
  <https://gitlab.hzdr.de/hifis/hifis-technical-documentation/-/jobs/artifacts/master/browse/public?job=build>
